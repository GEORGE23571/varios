package com.csform.android.uiapptemplate2.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class Wizards3Binding extends ViewDataBinding {
  @NonNull
  public final TextView miDescription;

  @NonNull
  public final ImageView miImage;

  @NonNull
  public final TextView miTitle;

  protected Wizards3Binding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView miDescription, ImageView miImage, TextView miTitle) {
    super(_bindingComponent, _root, _localFieldCount);
    this.miDescription = miDescription;
    this.miImage = miImage;
    this.miTitle = miTitle;
  }

  @NonNull
  public static Wizards3Binding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static Wizards3Binding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<Wizards3Binding>inflate(inflater, com.csform.android.uiapptemplate2.R.layout.wizards_3, root, attachToRoot, component);
  }

  @NonNull
  public static Wizards3Binding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static Wizards3Binding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<Wizards3Binding>inflate(inflater, com.csform.android.uiapptemplate2.R.layout.wizards_3, null, false, component);
  }

  public static Wizards3Binding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static Wizards3Binding bind(@NonNull View view, @Nullable DataBindingComponent component) {
    return (Wizards3Binding)bind(component, view, com.csform.android.uiapptemplate2.R.layout.wizards_3);
  }
}
