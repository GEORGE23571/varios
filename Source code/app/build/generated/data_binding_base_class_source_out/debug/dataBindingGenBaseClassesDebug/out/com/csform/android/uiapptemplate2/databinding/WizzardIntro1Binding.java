package com.csform.android.uiapptemplate2.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class WizzardIntro1Binding extends ViewDataBinding {
  @NonNull
  public final TextView miDescription;

  @NonNull
  public final ImageView miImage;

  @NonNull
  public final TextView miTitle;

  protected WizzardIntro1Binding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView miDescription, ImageView miImage, TextView miTitle) {
    super(_bindingComponent, _root, _localFieldCount);
    this.miDescription = miDescription;
    this.miImage = miImage;
    this.miTitle = miTitle;
  }

  @NonNull
  public static WizzardIntro1Binding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static WizzardIntro1Binding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<WizzardIntro1Binding>inflate(inflater, com.csform.android.uiapptemplate2.R.layout.wizzard_intro_1, root, attachToRoot, component);
  }

  @NonNull
  public static WizzardIntro1Binding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static WizzardIntro1Binding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<WizzardIntro1Binding>inflate(inflater, com.csform.android.uiapptemplate2.R.layout.wizzard_intro_1, null, false, component);
  }

  public static WizzardIntro1Binding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static WizzardIntro1Binding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (WizzardIntro1Binding)bind(component, view, com.csform.android.uiapptemplate2.R.layout.wizzard_intro_1);
  }
}
