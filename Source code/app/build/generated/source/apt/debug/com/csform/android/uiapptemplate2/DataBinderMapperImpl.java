package com.csform.android.uiapptemplate2;

import android.databinding.DataBinderMapper;
import android.databinding.DataBindingComponent;
import android.databinding.ViewDataBinding;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import com.csform.android.uiapptemplate2.databinding.Wizards1BindingImpl;
import com.csform.android.uiapptemplate2.databinding.Wizards1BindingLandImpl;
import com.csform.android.uiapptemplate2.databinding.Wizards2BindingImpl;
import com.csform.android.uiapptemplate2.databinding.Wizards2BindingLandImpl;
import com.csform.android.uiapptemplate2.databinding.Wizards3BindingImpl;
import com.csform.android.uiapptemplate2.databinding.Wizards3BindingLandImpl;
import com.csform.android.uiapptemplate2.databinding.WizzardIntro1BindingImpl;
import com.csform.android.uiapptemplate2.databinding.WizzardIntro1BindingLandImpl;
import com.csform.android.uiapptemplate2.databinding.WizzardIntro2BindingImpl;
import com.csform.android.uiapptemplate2.databinding.WizzardIntro2BindingLandImpl;
import com.csform.android.uiapptemplate2.databinding.WizzardIntro3BindingImpl;
import com.csform.android.uiapptemplate2.databinding.WizzardIntro3BindingLandImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_WIZARDS1 = 1;

  private static final int LAYOUT_WIZARDS2 = 2;

  private static final int LAYOUT_WIZARDS3 = 3;

  private static final int LAYOUT_WIZZARDINTRO1 = 4;

  private static final int LAYOUT_WIZZARDINTRO2 = 5;

  private static final int LAYOUT_WIZZARDINTRO3 = 6;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(6);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.csform.android.uiapptemplate2.R.layout.wizards_1, LAYOUT_WIZARDS1);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.csform.android.uiapptemplate2.R.layout.wizards_2, LAYOUT_WIZARDS2);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.csform.android.uiapptemplate2.R.layout.wizards_3, LAYOUT_WIZARDS3);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.csform.android.uiapptemplate2.R.layout.wizzard_intro_1, LAYOUT_WIZZARDINTRO1);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.csform.android.uiapptemplate2.R.layout.wizzard_intro_2, LAYOUT_WIZZARDINTRO2);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.csform.android.uiapptemplate2.R.layout.wizzard_intro_3, LAYOUT_WIZZARDINTRO3);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_WIZARDS1: {
          if ("layout-land/wizards_1_0".equals(tag)) {
            return new Wizards1BindingLandImpl(component, view);
          }
          if ("layout/wizards_1_0".equals(tag)) {
            return new Wizards1BindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for wizards_1 is invalid. Received: " + tag);
        }
        case  LAYOUT_WIZARDS2: {
          if ("layout-land/wizards_2_0".equals(tag)) {
            return new Wizards2BindingLandImpl(component, view);
          }
          if ("layout/wizards_2_0".equals(tag)) {
            return new Wizards2BindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for wizards_2 is invalid. Received: " + tag);
        }
        case  LAYOUT_WIZARDS3: {
          if ("layout-land/wizards_3_0".equals(tag)) {
            return new Wizards3BindingLandImpl(component, view);
          }
          if ("layout/wizards_3_0".equals(tag)) {
            return new Wizards3BindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for wizards_3 is invalid. Received: " + tag);
        }
        case  LAYOUT_WIZZARDINTRO1: {
          if ("layout-land/wizzard_intro_1_0".equals(tag)) {
            return new WizzardIntro1BindingLandImpl(component, view);
          }
          if ("layout/wizzard_intro_1_0".equals(tag)) {
            return new WizzardIntro1BindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for wizzard_intro_1 is invalid. Received: " + tag);
        }
        case  LAYOUT_WIZZARDINTRO2: {
          if ("layout-land/wizzard_intro_2_0".equals(tag)) {
            return new WizzardIntro2BindingLandImpl(component, view);
          }
          if ("layout/wizzard_intro_2_0".equals(tag)) {
            return new WizzardIntro2BindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for wizzard_intro_2 is invalid. Received: " + tag);
        }
        case  LAYOUT_WIZZARDINTRO3: {
          if ("layout/wizzard_intro_3_0".equals(tag)) {
            return new WizzardIntro3BindingImpl(component, view);
          }
          if ("layout-land/wizzard_intro_3_0".equals(tag)) {
            return new WizzardIntro3BindingLandImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for wizzard_intro_3 is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new com.android.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(2);

    static {
      sKeys.put(0, "_all");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(12);

    static {
      sKeys.put("layout-land/wizards_1_0", com.csform.android.uiapptemplate2.R.layout.wizards_1);
      sKeys.put("layout/wizards_1_0", com.csform.android.uiapptemplate2.R.layout.wizards_1);
      sKeys.put("layout-land/wizards_2_0", com.csform.android.uiapptemplate2.R.layout.wizards_2);
      sKeys.put("layout/wizards_2_0", com.csform.android.uiapptemplate2.R.layout.wizards_2);
      sKeys.put("layout-land/wizards_3_0", com.csform.android.uiapptemplate2.R.layout.wizards_3);
      sKeys.put("layout/wizards_3_0", com.csform.android.uiapptemplate2.R.layout.wizards_3);
      sKeys.put("layout-land/wizzard_intro_1_0", com.csform.android.uiapptemplate2.R.layout.wizzard_intro_1);
      sKeys.put("layout/wizzard_intro_1_0", com.csform.android.uiapptemplate2.R.layout.wizzard_intro_1);
      sKeys.put("layout-land/wizzard_intro_2_0", com.csform.android.uiapptemplate2.R.layout.wizzard_intro_2);
      sKeys.put("layout/wizzard_intro_2_0", com.csform.android.uiapptemplate2.R.layout.wizzard_intro_2);
      sKeys.put("layout/wizzard_intro_3_0", com.csform.android.uiapptemplate2.R.layout.wizzard_intro_3);
      sKeys.put("layout-land/wizzard_intro_3_0", com.csform.android.uiapptemplate2.R.layout.wizzard_intro_3);
    }
  }
}
