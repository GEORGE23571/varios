package com.csform.android.uiapptemplate2;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.adapter.TabsAdapter;
import com.ogaclejapan.smarttablayout.SmartTabLayout;


public class TabsLineIndicator extends BaseActivityTransparentToolbar {
    AppBarLayout appbar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orientation = 1;
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.tabs_lineindicator, contentFrameLayout);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(R.string.tabs_lineindicator_title);
      /*  toolbar = (Toolbar) findViewById(R.id.toolbar_transparent);
        toolbar.getBackground().setAlpha(0);
        //drawerLayout.setAlpha(1);
        appbar = (AppBarLayout)findViewById(R.id.app_bar_transparent);
        appbar.getBackground().setAlpha(0);*/


        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new TabsAdapter(this));
        viewPager.setOffscreenPageLimit(8);
        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setCustomTabView(new SmartTabLayout.TabProvider() {
            @Override
            public View createTabView(ViewGroup container, final int position, PagerAdapter adapter) {
                LayoutInflater inflater = LayoutInflater.from(container.getContext());
                Resources res = container.getContext().getResources();
                View tab = inflater.inflate(R.layout.custom_smarttab, container, false);
                TextView customText = (TextView) tab.findViewById(R.id.customTab);

                switch (position) {
                    case 0:

                        customText.setText(adapter.getPageTitle(position));
                        break;
                    case 1:

                        customText.setText(adapter.getPageTitle(position));
                        break;
                    case 2:

                        customText.setText(adapter.getPageTitle(position));
                        break;
                    case 3:

                        customText.setText(adapter.getPageTitle(position));
                        break;

                    default:
                        customText.setText(adapter.getPageTitle(position));
                        customText.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                viewPager.setCurrentItem(position);
                            }

                        });
                        break;
                }
                return tab;
            }
        });
        viewPagerTab.setViewPager(viewPager);

    }

}