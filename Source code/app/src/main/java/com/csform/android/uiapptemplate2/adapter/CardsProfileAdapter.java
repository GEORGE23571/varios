package com.csform.android.uiapptemplate2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.R;
import com.csform.android.uiapptemplate2.data.CardsProfileDataProvider;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;


public class CardsProfileAdapter extends RecyclerView.Adapter<com.csform.android.uiapptemplate2.adapter.CardsProfileAdapter.MyViewHolder> {

    private List<CardsProfileDataProvider> profilesList;


    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView card_title, card_subtitle, span_1, span_2, span_3;
        private TextView span_1_title, span_2_title, span_3_title;
        private ImageView avatar_image;
        public Context context;


        private MyViewHolder(final View view) {
            super(view);

            card_title = (TextView) view.findViewById(R.id.txtName);
            card_subtitle = (TextView) view.findViewById(R.id.txtAbout);
            span_1 = (TextView) view.findViewById(R.id.textNumber_1);
            span_2 = (TextView) view.findViewById(R.id.textNumber_2);
            span_3 = (TextView) view.findViewById(R.id.textNumber_3);
            avatar_image = (ImageView) view.findViewById(R.id.imgAvatar);
            span_1_title = (TextView) view.findViewById(R.id.span_1_title);
            span_2_title = (TextView) view.findViewById(R.id.span_2_title);
            span_3_title = (TextView) view.findViewById(R.id.span_3_title);


        }
    }


    public CardsProfileAdapter(List<CardsProfileDataProvider> profilesList) {
        this.profilesList = profilesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_profile, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        CardsProfileDataProvider profile = profilesList.get(position);
        holder.card_title.setText(profile.getCard_title());
        holder.card_subtitle.setText(profile.getCard_subtitle());


        StorageReference mStorageRef;

        if (holder.avatar_image.getTag() == null) {
            Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);


            if (profile.getPicture() != null) {

                holder.span_1.setText(String.format(Locale.getDefault(),"%d",profile.getPhotos()));
                holder.span_2.setText(String.format(Locale.getDefault(),"%d",profile.getFollowers()));
                holder.span_3.setText(String.format(Locale.getDefault(),"%d",profile.getFollowing()));
                holder.avatar_image.setImageResource(profile.getPicture());
            } else {
                holder.avatar_image.setImageDrawable(transparentDrawable);
                holder.avatar_image.setTag(R.drawable.ic_avatarshowman);

                holder.span_1.setText(profile.getSpan_1());
                holder.span_2.setText(profile.getSpan_2());
                holder.span_3.setText(profile.getSpan_3());
                holder.span_1_title.setText(profile.getSpan_1_title());
                holder.span_2_title.setText(profile.getSpan_2_title());
                holder.span_3_title.setText(profile.getSpan_3_title());
                if (profile.getAvatar_image_url() == null || profile.getAvatar_image_url().equals("")) {
                    if (profile.getAvatar_image_name() != null) {


                        if (!profile.getAvatar_image_name().replaceAll("\\s+", "").equals("")) {


                            mStorageRef = FirebaseStorage.getInstance().getReference();

                            mStorageRef.child("cards/profile/" + profile.getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                String result;

                                @Override
                                public void onSuccess(Uri uri) {

                                    String uristring = uri.toString();
                                    this.result = uristring;
                                    Picasso
                                            .with(holder.avatar_image.getContext())
                                            .load(uristring)
                                            .fit() // will explain later
                                            .into(holder.avatar_image);


                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    holder.avatar_image.setImageResource(R.drawable.default_image);


                                }
                            });

                        } else {

                            // holder.avatar_image.setVisibility(View.INVISIBLE);

                            holder.avatar_image.setImageDrawable(transparentDrawable);
                        }

                    } else {

                        // holder.avatar_image.setVisibility(View.INVISIBLE);

                        holder.avatar_image.setImageDrawable(transparentDrawable);
                    }

                } else {

                    Picasso
                            .with(holder.avatar_image.getContext())
                            .load(profile.getAvatar_image_url())
                          /*  .fit() // will explain later*/
                            .into(holder.avatar_image);
                }
            }

        }
    }

    @Override
    public int getItemCount() {
        return profilesList.size();
    }
}