package com.csform.android.uiapptemplate2;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.fragment.CardProfileFragment;



public class CardsProfile extends BaseActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(R.string.cards_profile_title);


        //prepareMovieData();


        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, new CardProfileFragment());
        ft.commit();


    }

}

