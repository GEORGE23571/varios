/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.csform.android.uiapptemplate2.Expandable;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.R;
import com.csform.android.uiapptemplate2.data.AbstractDataProviderExpandable;
import com.csform.android.uiapptemplate2.widget.ExpandableItemIndicator;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;
import com.squareup.picasso.Picasso;

class ListsExpandableAdapter
        extends AbstractExpandableItemAdapter<ListsExpandableAdapter.MyGroupViewHolder, ListsExpandableAdapter.MyChildViewHolder> {


    // NOTE: Make accessible with short card_title
    private interface Expandable extends ExpandableItemConstants {
    }

    private AbstractDataProviderExpandable mProvider;

    public static abstract class MyBaseViewHolder extends AbstractExpandableItemViewHolder {
        public FrameLayout mContainer;
        public TextView mTextName;
        public TextView mTextYear;
        public TextView mTextEmail;
        public TextView mChildName;
        public TextView mChildTelephone;
        public ImageView mAvatar;


        public MyBaseViewHolder(View v) {
            super(v);
            mContainer = (FrameLayout) v.findViewById(R.id.container);
            mTextYear = (TextView) v.findViewById(R.id.textGroupYear);
            mTextEmail = (TextView) v.findViewById(R.id.textGroupEmail);
            mTextName = (TextView) v.findViewById(R.id.textName);
            mAvatar = (ImageView) v.findViewById(R.id.imageAvatar);
            mChildName = (TextView) v.findViewById(R.id.textChildName);
            mChildTelephone = (TextView) v.findViewById(R.id.textChildTelephone);

        }
    }

    public static class MyGroupViewHolder extends MyBaseViewHolder {
        public ExpandableItemIndicator mIndicator;

        public MyGroupViewHolder(View v) {
            super(v);
            mIndicator = (ExpandableItemIndicator) v.findViewById(R.id.indicator);

        }
    }

    public static class MyChildViewHolder extends MyBaseViewHolder {
        public MyChildViewHolder(View v) {

            super(v);
        }
    }

    public ListsExpandableAdapter(AbstractDataProviderExpandable dataProvider) {
        mProvider = dataProvider;

        // ExpandableItemAdapter requires stable ID, and also
        // have to implement the getGroupItemId()/getChildItemId() methods appropriately.
        setHasStableIds(true);
    }

    public int getFirebase_done() {
        return mProvider.getFirebase_loaded();
    }

    @Override
    public int getGroupCount() {
        return mProvider.getGroupCount();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return mProvider.getChildCount(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return mProvider.getGroupItem(groupPosition).getGroupId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return mProvider.getChildItem(groupPosition, childPosition).getChildId();
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        return 0;
    }

    @Override
    public int getChildItemViewType(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public MyGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_group_item_expand, parent, false);
        return new MyGroupViewHolder(v);
    }

    @Override
    public MyChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_expand, parent, false);
        return new MyChildViewHolder(v);
    }

    @Override
    public void onBindGroupViewHolder(final MyGroupViewHolder holder, int groupPosition, int viewType) {
        // child item
        final AbstractDataProviderExpandable.BaseData item = mProvider.getGroupItem(groupPosition);
        StorageReference mStorageRef;

        // set text
        holder.mTextName.setText(item.getName());
        holder.mTextEmail.setText(item.getGroupEmail());
        holder.mTextYear.setText(item.getGroupYear());
        // holder.mAvatar.setImageResource(item.getGroupAvatar());


        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);

        if (item.getGroupAvatar() != null) {
            holder.mAvatar.setTag(R.drawable.ic_avatardiver);
            holder.mAvatar.setImageResource(item.getGroupAvatar());
        } else {

            if (item.getAvatar_image_url() == null || item.getAvatar_image_url().equals("")) {


                holder.mAvatar.setTag(R.drawable.ic_avatarshowman);
                if (item.getGroupAvatar_link() != null) {


                    if (!item.getGroupAvatar_link().replaceAll("\\s+", "").equals("")) {


                        mStorageRef = FirebaseStorage.getInstance().getReference();

                        mStorageRef.child("lists/expandable/" + item.getGroupAvatar_link()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            String result;

                            @Override
                            public void onSuccess(Uri uri) {

                                String uristring = uri.toString();
                                this.result = uristring;
                                Picasso
                                        .with(holder.mAvatar.getContext())
                                        .load(uristring)
                                        .fit() // will explain later
                                        .into(holder.mAvatar);


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                holder.mAvatar.setImageResource(R.drawable.default_image);


                            }
                        });

                    } else {


                        holder.mAvatar.setImageDrawable(transparentDrawable);
                    }

                } else {

                    holder.mAvatar.setImageDrawable(transparentDrawable);
                }

            } else {

                Picasso
                        .with(holder.mAvatar.getContext())
                        .load(item.getAvatar_image_url())
                        .fit() // will explain later
                        .into(holder.mAvatar);
            }
        }


        // mark as clickable
        holder.itemView.setClickable(true);

        // set background resource (target view ID: container)
        final int expandState = holder.getExpandStateFlags();

        if ((expandState & ExpandableItemConstants.STATE_FLAG_IS_UPDATED) != 0) {

            boolean isExpanded;
            boolean animateIndicator = ((expandState & Expandable.STATE_FLAG_HAS_EXPANDED_STATE_CHANGED) != 0);

            isExpanded = (expandState & Expandable.STATE_FLAG_IS_EXPANDED) != 0;


            holder.mIndicator.setExpandedState(isExpanded, animateIndicator);
        }
    }

    @Override
    public void onBindChildViewHolder(MyChildViewHolder holder, int groupPosition, int childPosition, int viewType) {
        // group item
        final AbstractDataProviderExpandable.ChildData item = mProvider.getChildItem(groupPosition, childPosition);

        // set text
        // holder.mTextView.setText(item.getText());
        System.out.println(item.getName());
        System.out.println("opa");
        System.out.println(item.getChildTelephone());
        holder.mChildName.setText(item.getName());
        holder.mChildTelephone.setText(item.getChildTelephone());



    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(MyGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        // check the item is *not* pinned
        if (mProvider.getGroupItem(groupPosition).isPinned()) {
            // return false to raise View.OnClickListener#onClick() event
            return false;
        }

        // check is enabled
        return holder.itemView.isEnabled() && holder.itemView.isClickable();
    }
}
