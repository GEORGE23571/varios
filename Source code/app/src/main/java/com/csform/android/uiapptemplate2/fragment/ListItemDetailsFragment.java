package com.csform.android.uiapptemplate2.fragment;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.csform.android.uiapptemplate2.ListItemDetailsSingle;
import com.csform.android.uiapptemplate2.R;
import com.csform.android.uiapptemplate2.RecyclerItemClickListener;
import com.csform.android.uiapptemplate2.adapter.ListItemDetailsAdapter;
import com.csform.android.uiapptemplate2.data.ListItemDetailsDataProvider;
import com.csform.android.uiapptemplate2.utils.HidingScrollListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;



public class ListItemDetailsFragment extends Fragment {

    private List<ListItemDetailsDataProvider> itemDetails = new ArrayList<>();
    final ArrayList<ListItemDetailsDataProvider> theItemDetailsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ListItemDetailsAdapter mAdapter;
    private StorageReference mStorageRef;
    private Context context;
    private String passingData[][];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        // getLayoutInflater().inflate(R.layout.recyclerview_card_profile, contentFrameLayout);
        return inflater.inflate(R.layout.lists_item_details, parent, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        this.context = getActivity().getApplicationContext();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        SharedPreferences subscribe = getActivity().getApplicationContext().getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        int firebaseStatus = subscribe.getInt("firebase_enabled", 0);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(context, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getActivity(), ListItemDetailsSingle.class);
                        if (passingData != null) {
                            String[] data = passingData[position];
                            Bitmap bmp = getBitmapFromURL(data[2]);
                            try {
                                String filename = "bitmap.png";
                                try {
                                    FileOutputStream stream = getContext().openFileOutput(filename, getActivity().MODE_PRIVATE);
                                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);

                                    stream.close();
                                    bmp.recycle();
                                    intent.putExtra("image", filename);
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            intent.putExtra("data", data);
                        }
                        intent.putExtra("position", position);
                        View transitionView = view.findViewById(R.id.year);
                        //context.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation
                        //         (ListItemDetails.class,sharedelement , "shareView").toBundle());
                        //startActivity(intent);
                       /*RelativeLayout rela_round = (RelativeLayout) view.findViewById(R.id.rela_round);
                        Intent intent = new Intent(ListItemDetails.this, ListItemDetails.class);
                        // create the transition animation - the images in the layouts
                        // of both activities are defined with android:transitionName="robot"*/
                        ActivityOptions options = ActivityOptions
                                .makeSceneTransitionAnimation(getActivity(), transitionView, "shareView"+position);
                        System.out.println("ola");
                        System.out.println("shareView"+position);
                        // start the new activity
                        startActivity(intent, options.toBundle());
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        if (firebaseStatus == 1) {

            mStorageRef = FirebaseStorage.getInstance().getReference();
            final FirebaseDatabase database1 = FirebaseDatabase.getInstance();

            DatabaseReference ref1 = database1.getReference("Lists/item_details");

            ref1.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                    ListItemDetailsDataProvider itemDetail = dataSnapshot.getValue(ListItemDetailsDataProvider.class);


                    theItemDetailsList.add(itemDetail);

                    //setCategoriesData(count);
                    //count++;

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            ref1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                                        int count = (int) dataSnapshot.getChildrenCount();
                                                        passingData = new String[count][9];
                                                        for (int i = 0; i < count; i++) {
                                                            final int iterration = i;
                                                            passingData[i][0] = theItemDetailsList.get(i).getItem_title();
                                                            passingData[i][1] = theItemDetailsList.get(i).getItem_subtitle();
                                                            if (theItemDetailsList.get(i).getAvatar_image_url() == null || theItemDetailsList.get(i).getAvatar_image_url().equals("")) {
                                                                passingData[i][2] = theItemDetailsList.get(i).getAvatar_image_name();
                                                                mStorageRef = FirebaseStorage.getInstance().getReference();

                                                                mStorageRef.child("lists/item_details/" + theItemDetailsList.get(i).getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {


                                                                    @Override
                                                                    public void onSuccess(Uri uri) {

                                                                        String uristring = uri.toString();
                                                                        passingData[iterration][2] = uristring;


                                                                    }
                                                                }).addOnFailureListener(new OnFailureListener() {
                                                                    @Override
                                                                    public void onFailure(@NonNull Exception exception) {
                                                                        passingData[iterration][2] = null;


                                                                    }
                                                                });
                                                            } else {
                                                                passingData[i][2] = theItemDetailsList.get(i).getAvatar_image_url();
                                                            }


                                                            passingData[i][3] = theItemDetailsList.get(i).getSpan_1_title();
                                                            passingData[i][4] = theItemDetailsList.get(i).getSpan_1();
                                                            passingData[i][5] = theItemDetailsList.get(i).getSpan_2_title();
                                                            passingData[i][6] = theItemDetailsList.get(i).getSpan_2();
                                                            passingData[i][7] = theItemDetailsList.get(i).getSpan_3_title();
                                                            passingData[i][8] = theItemDetailsList.get(i).getSpan_3();
                                                        }
                                                        itemDetails = theItemDetailsList;
                                                        mAdapter = new ListItemDetailsAdapter(itemDetails);
                                                        recyclerView.setAdapter(null);
                                                        recyclerView.setAdapter(mAdapter);
                                                        //recyclerView.getAdapter().notifyDataSetChanged();

                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                }
            );
        } else {
            mAdapter = new ListItemDetailsAdapter(itemDetails);
            prepareItemDetailsData();
            recyclerView.setAdapter(mAdapter);
        }
        final FloatingActionButton fam = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fam.setVisibility(View.VISIBLE);
        final Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        recyclerView.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fam.getLayoutParams();
                int fabBottomMargin = lp.bottomMargin;
                fam.animate().translationY(fam.getHeight() + fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }

            @Override
            public void onShow() {
                toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
                fam.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }
        });




    }

    private void prepareItemDetailsData() {
        ListItemDetailsDataProvider item = new ListItemDetailsDataProvider("Grant Marshall", "Warsaw", R.drawable.ic_avatarcaptain, "7186", "Photos", "40", "Followers", "58", "Following");
        itemDetails.add(item);

        item = new ListItemDetailsDataProvider("Pena Valdez", "Dunnavant", R.drawable.ic_avatarshowman, "2281", "Photos", "58", "Followers", "16", "Following");
        itemDetails.add(item);

        item = new ListItemDetailsDataProvider("Jessica Miles", "Ryderwood", R.drawable.ic_avatardisc_jockey, "1748", "Photos", "55", "Followers", "92", "Following");
        itemDetails.add(item);
        item = new ListItemDetailsDataProvider("Kerri Barber", "Enlow", R.drawable.ic_avatarastronaut, "3450", "Photos", "41", "Followers", "93", "Following");
        itemDetails.add(item);

        item = new ListItemDetailsDataProvider("Natasha Gamble", "Ferney", R.drawable.ic_avatardetective, "7374", "Photos", "48", "Followers", "43", "Following");
        itemDetails.add(item);

        item = new ListItemDetailsDataProvider("White Castaneda", "Lithium", R.drawable.ic_avatardisc_jockey, "6070", "Photos", "70", "Followers", "62", "Following");
        itemDetails.add(item);

        item = new ListItemDetailsDataProvider("Vanessa Ryan", "Como", R.drawable.ic_avatardiver, "8158", "Photos", "6", "Followers", "81", "Following");
        itemDetails.add(item);

        item = new ListItemDetailsDataProvider("Meredith Hendricks", "Carrizo", R.drawable.ic_avatardetective, "292", "Photos", "85", "Followers", "2", "Following");
        itemDetails.add(item);

        item = new ListItemDetailsDataProvider("Carol Kelly", "Brewster", R.drawable.ic_avatardiver, "9231", "Photos", "59", "Followers", "4", "Following");
        itemDetails.add(item);

        item = new ListItemDetailsDataProvider("Barrera Ramsey", "Fowlerville", R.drawable.ic_avatarshowman, "9972", "Photos", "24", "Followers", "51", "Following");
        itemDetails.add(item);


        mAdapter.notifyDataSetChanged();
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
}