package com.csform.android.uiapptemplate2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;



public class DialogWarning extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_warning);
        this.setFinishOnTouchOutside(false);
        Intent intent = getIntent();
        String value = intent.getStringExtra("key"); //if it's a string you stored
        if (value != null && value.equals("internet")) {
            TextView dialogtext = (TextView) findViewById(R.id.dialog_info_text);
            dialogtext.setText("Please connect to internet. Otherwise application will not download needed content");
        }


        final TextView subscribe = (TextView) findViewById(R.id.dialog_warning_ok);

        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
