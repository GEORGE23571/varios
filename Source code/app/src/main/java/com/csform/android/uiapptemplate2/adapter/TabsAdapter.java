package com.csform.android.uiapptemplate2.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.csform.android.uiapptemplate2.R;
import com.csform.android.uiapptemplate2.TabObject;
import com.csform.android.uiapptemplate2.fragment.CardFollowFragment;
import com.csform.android.uiapptemplate2.fragment.CardProfileFragment;
import com.csform.android.uiapptemplate2.fragment.CardRateFragment;


public class TabsAdapter extends PagerAdapter {

    private Context mContext;

    public TabsAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        TabObject tabObject = TabObject.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);

        ViewGroup layout = (ViewGroup) inflater.inflate(tabObject.getLayoutResId(), collection, false);
        if (tabObject.getLayoutResId() == R.layout.view_blue) {

            final FragmentTransaction fb = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fb.replace(R.id.content_frame_blue, new CardFollowFragment());
                    fb.commit();
                }
            }, 0);


        }
        if (tabObject.getLayoutResId() == R.layout.view_red) {


            final FragmentTransaction fr = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fr.replace(R.id.content_frame_red, new CardProfileFragment());
                    fr.commit();
                }
            }, 0);
        }
        if (tabObject.getLayoutResId() == R.layout.view_green) {


            final FragmentTransaction fg = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fg.replace(R.id.content_frame_green, new CardRateFragment());
                    fg.commit();
                }
            }, 0);

        }


        if (tabObject.getLayoutResId() == R.layout.view_blue_1) {

            final FragmentTransaction fb1 = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fb1.replace(R.id.content_frame_blue1, new CardFollowFragment());
                    fb1.commit();
                }
            }, 0);


        }
        if (tabObject.getLayoutResId() == R.layout.view_red_1) {


            final FragmentTransaction fr1 = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fr1.replace(R.id.content_frame_red1, new CardProfileFragment());
                    fr1.commit();
                }
            }, 0);
        }
        if (tabObject.getLayoutResId() == R.layout.view_green_1) {


            final FragmentTransaction fg1 = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fg1.replace(R.id.content_frame_green1, new CardRateFragment());
                    fg1.commit();
                }
            }, 0);

        }

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return TabObject.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        TabObject customPagerEnum = TabObject.values()[position];

        return mContext.getString(customPagerEnum.getTitleResId());
    }

}