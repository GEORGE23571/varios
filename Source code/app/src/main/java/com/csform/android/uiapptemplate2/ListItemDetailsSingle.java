package com.csform.android.uiapptemplate2;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.data.ListItemDetailsDataProvider;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;



public class ListItemDetailsSingle extends BaseActivity {
    private List<ListItemDetailsDataProvider> itemSingleList = new ArrayList<>();
    String[] data;
    int temp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.list_item_details_single, contentFrameLayout);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(R.string.lists_item_details_title);
        final ImageView avatar = (ImageView) findViewById(R.id.imgAvatar);
        TextView name = (TextView) findViewById(R.id.txtName);
        TextView about = (TextView) findViewById(R.id.txtAbout);

        TextView span_1_title = (TextView) findViewById(R.id.textPhotos);
        TextView span_2_title = (TextView) findViewById(R.id.textFollowers);
        TextView span_3_title = (TextView) findViewById(R.id.textFollowing);

        TextView span_1 = (TextView) findViewById(R.id.numPhotos);
        TextView span_2 = (TextView) findViewById(R.id.numFollowers);
        TextView span_3 = (TextView) findViewById(R.id.numFollowing);
        Intent intent = getIntent();
        temp = intent.getIntExtra("position", 0); //
        data = intent.getStringArrayExtra("data");
        if (data != null) {

            name.setText(data[0]);

            about.setText(data[1]);
            Bitmap bmp = null;
            String filename = getIntent().getStringExtra("image");
            try {
                FileInputStream is = this.openFileInput(filename);
                bmp = BitmapFactory.decodeStream(is);
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            avatar.setImageBitmap(bmp);
            avatar.setTransitionName("shareView"+temp);
            System.out.println("aaa");
            System.out.println("shareView"+temp);

            span_1_title.setText(data[3]);
            span_1.setText(data[4]);
            span_2_title.setText(data[5]);
            span_2.setText(data[6]);
            span_3_title.setText(data[7]);
            span_3.setText(data[8]);


        } else {


            prepareItemSingleData();
            System.out.println("shareView"+temp);
            avatar.setTransitionName("shareView"+temp);
            ListItemDetailsDataProvider movie = itemSingleList.get(temp);
            avatar.setImageResource(movie.getAvatar_image());
           // avatar.setTransitionName("shareView"+temp);
            name.setText(movie.getItem_title());
            about.setText(movie.getItem_subtitle());
            span_1.setText(movie.getSpan_1());
            span_2.setText(movie.getSpan_2());
            span_3.setText(movie.getSpan_3());
        }


    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, ListItemDetails.class);
        View transitionView = findViewById(R.id.imgAvatar);

        //context.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation
        //         (ListItemDetails.class,sharedelement , "shareView").toBundle());
        //startActivity(intent);
                       /*RelativeLayout rela_round = (RelativeLayout) view.findViewById(R.id.rela_round);
                        Intent intent = new Intent(ListItemDetails.this, ListItemDetails.class);
                        // create the transition animation - the images in the layouts
                        // of both activities are defined with android:transitionName="robot"*/
        ActivityOptions options = ActivityOptions
                .makeSceneTransitionAnimation(this, transitionView, "shareView"+temp);
        // start the new activity
        startActivity(intent, options.toBundle());



        startActivity(intent);
       // finish();

        // code here to show dialog
      //  super.onBackPressed();  // optional depending on your needs
    }

    private void prepareItemSingleData() {
        ListItemDetailsDataProvider itemSingle = new ListItemDetailsDataProvider("Grant Marshall", "Warsaw", R.drawable.ic_avatarcaptain, "7186", "Photos", "40", "Followers", "58", "Following");
        itemSingleList.add(itemSingle);

        itemSingle = new ListItemDetailsDataProvider("Pena Valdez", "Dunnavant", R.drawable.ic_avatarshowman, "2281", "Photos", "58", "Followers", "16", "Following");
        itemSingleList.add(itemSingle);

        itemSingle = new ListItemDetailsDataProvider("Jessica Miles", "Ryderwood", R.drawable.ic_avatardisc_jockey, "1748", "Photos", "55", "Followers", "92", "Following");
        itemSingleList.add(itemSingle);
        itemSingle = new ListItemDetailsDataProvider("Kerri Barber", "Enlow", R.drawable.ic_avatarastronaut, "3450", "Photos", "41", "Followers", "93", "Following");
        itemSingleList.add(itemSingle);

        itemSingle = new ListItemDetailsDataProvider("Natasha Gamble", "Ferney", R.drawable.ic_avatardetective, "7374", "Photos", "48", "Followers", "43", "Following");
        itemSingleList.add(itemSingle);

        itemSingle = new ListItemDetailsDataProvider("White Castaneda", "Lithium", R.drawable.ic_avatardisc_jockey, "6070", "Photos", "70", "Followers", "62", "Following");
        itemSingleList.add(itemSingle);

        itemSingle = new ListItemDetailsDataProvider("Vanessa Ryan", "Como", R.drawable.ic_avatardiver, "8158", "Photos", "6", "Followers", "81", "Following");
        itemSingleList.add(itemSingle);

        itemSingle = new ListItemDetailsDataProvider("Meredith Hendricks", "Carrizo", R.drawable.ic_avatardetective, "292", "Photos", "85", "Followers", "2", "Following");
        itemSingleList.add(itemSingle);

        itemSingle = new ListItemDetailsDataProvider("Carol Kelly", "Brewster", R.drawable.ic_avatardiver, "9231", "Photos", "59", "Followers", "4", "Following");
        itemSingleList.add(itemSingle);

        itemSingle = new ListItemDetailsDataProvider("Barrera Ramsey", "Fowlerville", R.drawable.ic_avatarshowman, "9972", "Photos", "24", "Followers", "51", "Following");
        itemSingleList.add(itemSingle);


    }
}