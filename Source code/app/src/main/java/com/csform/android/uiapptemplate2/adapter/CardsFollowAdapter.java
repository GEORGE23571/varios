package com.csform.android.uiapptemplate2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.R;
import com.csform.android.uiapptemplate2.data.CardsFollowDataProvider;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.List;




public class CardsFollowAdapter extends RecyclerView.Adapter<CardsFollowAdapter.MyViewHolder> {

    private List<CardsFollowDataProvider> followList;


    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView card_title, card_subtitle;
        private ImageView avatar_image_name_1, avatar_image_name_2, avatar_image_name_3, avatar_image_name_4;
        public Context context;


        private MyViewHolder(final View view) {
            super(view);

            card_title = (TextView) view.findViewById(R.id.txtName);
            card_subtitle = (TextView) view.findViewById(R.id.txtAbout);


            avatar_image_name_1 = (ImageView) view.findViewById(R.id.picture1);
            avatar_image_name_2 = (ImageView) view.findViewById(R.id.picture2);
            avatar_image_name_3 = (ImageView) view.findViewById(R.id.picture3);
            avatar_image_name_4 = (ImageView) view.findViewById(R.id.picture4);

            Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
            avatar_image_name_1.setImageDrawable(transparentDrawable);
            avatar_image_name_2.setImageDrawable(transparentDrawable);
            avatar_image_name_3.setImageDrawable(transparentDrawable);
            avatar_image_name_4.setImageDrawable(transparentDrawable);


        }
    }


    public CardsFollowAdapter(List<CardsFollowDataProvider> followList) {
        this.followList = followList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_follow, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final CardsFollowDataProvider follow = followList.get(position);
        holder.card_title.setText(follow.getCard_title().toUpperCase());
        holder.card_subtitle.setText(follow.getCard_subtitle());

        StorageReference mStorageRef;
        if (follow.getPicture1() != null) {

            holder.avatar_image_name_1.setImageResource(follow.getPicture1());
            holder.avatar_image_name_2.setImageResource(follow.getPicture2());
            holder.avatar_image_name_3.setImageResource(follow.getPicture3());
            holder.avatar_image_name_4.setImageResource(follow.getPicture4());


        } else {
            if (follow.getAvatar_image_name_1_url() == null || follow.getAvatar_image_name_1_url().equals("")) {
                if (follow.getAvatar_image_name_1() != null) {


                    if (!follow.getAvatar_image_name_1().replaceAll("\\s+", "").equals("")) {


                        mStorageRef = FirebaseStorage.getInstance().getReference();

                        mStorageRef.child("cards/follow/" + follow.getAvatar_image_name_1()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            String result;

                            @Override
                            public void onSuccess(Uri uri) {

                                String uristring = uri.toString();
                                this.result = uristring;
                                Picasso
                                        .with(holder.avatar_image_name_1.getContext())
                                        .load(uristring)
                                        .fit() // will explain later
                                        .into(holder.avatar_image_name_1);


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                holder.avatar_image_name_1.setImageResource(R.drawable.default_image);


                            }
                        });

                    } else {

                        // holder.avatar_image.setVisibility(View.INVISIBLE);
                        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
                        holder.avatar_image_name_1.setImageDrawable(transparentDrawable);
                    }

                } else {

                    // holder.avatar_image.setVisibility(View.INVISIBLE);
                    Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
                    holder.avatar_image_name_1.setImageDrawable(transparentDrawable);
                }
            } else {
                Picasso
                        .with(holder.avatar_image_name_1.getContext())
                        .load(follow.getAvatar_image_name_1_url())
                        .fit() // will explain later
                        .into(holder.avatar_image_name_1);
            }


            if (follow.getAvatar_image_name_2_url() == null || follow.getAvatar_image_name_2_url().equals("")) {
                if (follow.getAvatar_image_name_2() != null) {


                    if (!follow.getAvatar_image_name_2().replaceAll("\\s+", "").equals("")) {

                        mStorageRef = FirebaseStorage.getInstance().getReference();

                        mStorageRef.child("cards/follow/" + follow.getAvatar_image_name_2()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            String result;

                            @Override
                            public void onSuccess(Uri uri) {

                                String uristring = uri.toString();
                                this.result = uristring;
                                Picasso
                                        .with(holder.avatar_image_name_2.getContext())
                                        .load(uristring)
                                        .fit() // will explain later
                                        .into(holder.avatar_image_name_2);


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                holder.avatar_image_name_2.setImageResource(R.drawable.default_image);


                            }
                        });

                    } else {

                        // holder.avatar_image.setVisibility(View.INVISIBLE);
                        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
                        holder.avatar_image_name_2.setImageDrawable(transparentDrawable);
                    }

                } else {

                    // holder.avatar_image.setVisibility(View.INVISIBLE);
                    Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
                    holder.avatar_image_name_2.setImageDrawable(transparentDrawable);
                }
            } else {
                Picasso
                        .with(holder.avatar_image_name_2.getContext())
                        .load(follow.getAvatar_image_name_2_url())
                        .fit() // will explain later
                        .into(holder.avatar_image_name_2);
            }

            if (follow.getAvatar_image_name_3_url() == null || follow.getAvatar_image_name_3_url().equals("")) {
                if (follow.getAvatar_image_name_3() != null) {


                    if (!follow.getAvatar_image_name_3().replaceAll("\\s+", "").equals("")) {


                        mStorageRef = FirebaseStorage.getInstance().getReference();

                        mStorageRef.child("cards/follow/" + follow.getAvatar_image_name_3()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            String result;

                            @Override
                            public void onSuccess(Uri uri) {

                                String uristring = uri.toString();
                                this.result = uristring;
                                Picasso
                                        .with(holder.avatar_image_name_3.getContext())
                                        .load(uristring)
                                        .fit() // will explain later
                                        .into(holder.avatar_image_name_3);


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                holder.avatar_image_name_3.setImageResource(R.drawable.default_image);


                            }
                        });

                    } else {

                        // holder.avatar_image.setVisibility(View.INVISIBLE);
                        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
                        holder.avatar_image_name_3.setImageDrawable(transparentDrawable);
                    }

                } else {

                    // holder.avatar_image.setVisibility(View.INVISIBLE);
                    Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
                    holder.avatar_image_name_3.setImageDrawable(transparentDrawable);
                }
            } else {
                Picasso
                        .with(holder.avatar_image_name_3.getContext())
                        .load(follow.getAvatar_image_name_3_url())
                        .fit() // will explain later
                        .into(holder.avatar_image_name_3);
            }


            if (follow.getAvatar_image_name_4_url() == null || follow.getAvatar_image_name_4_url().equals("")) {
                if (follow.getAvatar_image_name_4() != null) {


                    if (!follow.getAvatar_image_name_4().replaceAll("\\s+", "").equals("")) {


                        mStorageRef = FirebaseStorage.getInstance().getReference();

                        mStorageRef.child("cards/follow/" + follow.getAvatar_image_name_4()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            String result;

                            @Override
                            public void onSuccess(Uri uri) {

                                String uristring = uri.toString();
                                this.result = uristring;
                                Picasso
                                        .with(holder.avatar_image_name_4.getContext())
                                        .load(uristring)
                                        .fit() // will explain later
                                        .into(holder.avatar_image_name_4);


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                holder.avatar_image_name_4.setImageResource(R.drawable.default_image);


                            }
                        });

                    } else {

                        // holder.avatar_image.setVisibility(View.INVISIBLE);
                        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
                        holder.avatar_image_name_4.setImageDrawable(transparentDrawable);
                    }

                } else {

                    // holder.avatar_image.setVisibility(View.INVISIBLE);
                    Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
                    holder.avatar_image_name_4.setImageDrawable(transparentDrawable);
                }

            } else {
                Picasso
                        .with(holder.avatar_image_name_4.getContext())
                        .load(follow.getAvatar_image_name_4_url())
                        .fit() // will explain later
                        .into(holder.avatar_image_name_4);
            }
        }


    }

    @Override
    public int getItemCount() {
        return followList.size();
    }
}