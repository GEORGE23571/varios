/*
 * MIT License
 *
 * Copyright (c) 2017 Jan Heinrich Reimer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.csform.android.uiapptemplate2;

import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

public class WizzardIntro extends IntroActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setButtonBackVisible(false);
        setButtonNextVisible(false);
        setButtonCtaVisible(true);
        setButtonCtaTintMode(BUTTON_CTA_TINT_MODE_BACKGROUND);
        TypefaceSpan labelSpan = new TypefaceSpan(
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? "sans-serif-medium" : "sans serif");
        SpannableString label = SpannableString
                .valueOf("BUY NOW");
        label.setSpan(labelSpan, 0, label.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        setButtonCtaLabel(label);
        setButtonCtaTintMode(2);


        setPageScrollDuration(500);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setPageScrollInterpolator(android.R.interpolator.fast_out_slow_in);
        }

        addSlide(new SimpleSlide.Builder()
                .title(R.string.wizzard_intro_1_title)
                .description(R.string.wizzard_intro_1_text)
                .image(R.drawable.ic_smartphone)
                .background(R.color.colorTextDark)
                .backgroundDark(R.color.colorTextDark)
                .layout(R.layout.wizzard_intro_1)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title(R.string.wizzard_intro_2_title)
                .description(R.string.wizzard_intro_2_text)
                .image(R.drawable.ic_hourglass)
                .background(R.color.colorTextDark)
                .backgroundDark(R.color.colorTextDark)

                .layout(R.layout.wizzard_intro_2)
                .build());


        addSlide(new SimpleSlide.Builder()
                .title(R.string.wizzard_intro_3_title)
                .description(R.string.wizzard_intro_3_text)
                .image(R.drawable.ic_magic_wand_2)
                .background(R.color.colorTextDark)
                .backgroundDark(R.color.colorTextDark)
                .layout(R.layout.wizzard_intro_3)
                .build());

        autoplay(2500, INFINITE);


        /*final Handler h = new Handler();
        final int delay = 1; //milliseconds

        h.postDelayed(new Runnable(){
            public void run(){
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

                h.postDelayed(this, delay);
            }
        }, delay);*/
    }


}
