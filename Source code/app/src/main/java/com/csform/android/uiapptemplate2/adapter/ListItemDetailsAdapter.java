package com.csform.android.uiapptemplate2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.R;
import com.csform.android.uiapptemplate2.data.ListItemDetailsDataProvider;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.List;



public class ListItemDetailsAdapter extends RecyclerView.Adapter<ListItemDetailsAdapter.MyViewHolder> {

    private List<ListItemDetailsDataProvider> moviesList;


    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title, genre;
        private ImageView year;
        public Context context;


        private MyViewHolder(final View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            year = (ImageView) view.findViewById(R.id.year);


        }
    }


    public ListItemDetailsAdapter(List<ListItemDetailsDataProvider> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.test_list_item_detail_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        StorageReference mStorageRef;
        holder.title.setText("");
        holder.genre.setText("");
        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
        holder.year.setImageDrawable(transparentDrawable);
        holder.year.setTransitionName("shareView"+position);

        ListItemDetailsDataProvider movie = moviesList.get(position);
        holder.title.setText(movie.getItem_title());
        holder.genre.setText(movie.getItem_subtitle());
        if (movie.getAvatar_image() != null) {

            holder.year.setImageResource(movie.getAvatar_image());
        } else {

            if (movie.getAvatar_image_url() == null || movie.getAvatar_image_url().equals("")) {

                if (movie.getAvatar_image_name() != null) {


                    if (!movie.getAvatar_image_name().replaceAll("\\s+", "").equals("")) {


                        mStorageRef = FirebaseStorage.getInstance().getReference();

                        mStorageRef.child("lists/item_details/" + movie.getAvatar_image_name()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            String result;

                            @Override
                            public void onSuccess(Uri uri) {

                                String uristring = uri.toString();
                                this.result = uristring;
                                Picasso
                                        .with(holder.year.getContext())
                                        .load(uristring)
                                        .fit() // will explain later
                                        .into(holder.year);


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                holder.year.setImageResource(R.drawable.default_image);


                            }
                        });

                    } else {

                        holder.year.setImageDrawable(transparentDrawable);
                    }

                }
            } else {

                Picasso
                        .with(holder.year.getContext())
                        .load(movie.getAvatar_image_url())
                        .fit() // will explain later
                        .into(holder.year);
            }
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}

