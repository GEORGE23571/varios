/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.csform.android.uiapptemplate2;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.csform.android.uiapptemplate2.Swipeable.ListsSwipeableWithButtonFragment;
import com.csform.android.uiapptemplate2.data.AbstractDataProviderSwipeable;
import com.csform.android.uiapptemplate2.fragment.ItemPinnedMessageDialogFragment;
import com.csform.android.uiapptemplate2.fragment.SwipeableDataProviderFragment;


public class ListSwipeableWithButton extends BaseActivity implements ItemPinnedMessageDialogFragment.EventListener {
    private static final String FRAGMENT_TAG_DATA_PROVIDER = "data provider";
    private static final String FRAGMENT_LIST_VIEW = "list view";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.activity_demo, contentFrameLayout);
        TextView toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        toolbartitle.setText(R.string.lists_swipe_title);


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(new SwipeableDataProviderFragment(), FRAGMENT_TAG_DATA_PROVIDER)
                    .commit();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new ListsSwipeableWithButtonFragment(), FRAGMENT_LIST_VIEW)
                    .commit();
        }
    }

    /**
     * This method will be called when a list item is pinned
     *
     * @param position The position of the item within data set
     */
    public void onItemPinned(int position) {
        ///was empty before gk


        int size = getDataProvider().getCount();

        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);

        for (int i = 0; i < size; i++) {
            AbstractDataProviderSwipeable.Data data = getDataProvider().getItem(i);
            if (i != position) {

                if (data.isPinned()) {

                    // unpin if tapped the pinned item
                    data.setPinned(false);


                    ((ListsSwipeableWithButtonFragment) fragment).notifyItemChanged(i);
                }
            }


        }


    }

    /**
     * This method will be called when a list item is clicked
     *
     * @param position The position of the item within data set
     */
    public void onItemClicked(int position) {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);
        AbstractDataProviderSwipeable.Data data = getDataProvider().getItem(position);


        if (data.isPinned()) {
            // unpin if tapped the pinned item
            data.setPinned(false);

            ((ListsSwipeableWithButtonFragment) fragment).notifyItemChanged(position);
        } else {
            data.setPinned(false);
        }
    }

    /**
     * This method will be called when a "button placed under the swipeable view" is clicked
     *
     * @param position The position of the item within data set
     */
    public void onItemButtonClicked(int position) {



        AbstractDataProviderSwipeable.Data data = getDataProvider().getItem(position);
        if (data != null) {
            if (data.isPinned()) {
                data.setPinned(false);

            }
        }

        String text = getString(R.string.snack_bar_text_button_clicked, position);


        Snackbar snackbar = Snackbar.make(
                findViewById(R.id.container),
                text,
                Snackbar.LENGTH_SHORT);

        snackbar.show();
    }

    public void onItemButtonClicked1(int position) {
        String text = getString(R.string.snack_bar_text_button_clicked, position);


        Snackbar snackbar = Snackbar.make(
                findViewById(R.id.container),
                text,
                Snackbar.LENGTH_SHORT);

        snackbar.show();
    }

    // implements ItemPinnedMessageDialogFragment.EventListener
    @Override
    public void onNotifyItemPinnedDialogDismissed(int itemPosition, boolean ok) {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);

        getDataProvider().getItem(itemPosition).setPinned(ok);
        ((ListsSwipeableWithButtonFragment) fragment).notifyItemChanged(itemPosition);
    }

    public AbstractDataProviderSwipeable getDataProvider() {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER);
        return ((SwipeableDataProviderFragment) fragment).getDataProvider();
    }
}
