/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.csform.android.uiapptemplate2.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.csform.android.uiapptemplate2.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import java.util.LinkedList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class DraggableDataProvider extends AbstractDataProviderDraggable {
    private List<ConcreteData> mData;
    private Context context;
    private Integer firebase_enabled = 0;
    private volatile Integer firebase_loaded;
    private ConcreteData mLastRemovedData;
    private int mLastRemovedPosition = -1;
    private StorageReference mStorageRef;


    public DraggableDataProvider(Context context) {

        this.context = context;

        SharedPreferences subscribe = context.getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        firebase_enabled = subscribe.getInt("firebase_enabled", 0);

        mData = new LinkedList<>();
        final int swipeReaction = RecyclerViewSwipeManager.REACTION_CAN_SWIPE_UP | RecyclerViewSwipeManager.REACTION_CAN_SWIPE_DOWN;
        final int viewType = 0;


        if (firebase_enabled == 1) {

            firebase_loaded = 0;
            mStorageRef = FirebaseStorage.getInstance().getReference();
            final FirebaseDatabase database1 = FirebaseDatabase.getInstance();

            DatabaseReference ref1 = database1.getReference("Lists/drag_drop");

            ref1.addChildEventListener(new ChildEventListener() {
                int count = 0;

                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    ListData listData = dataSnapshot.getValue(ListData.class);

                    mData.add(new ConcreteData(count, viewType, listData.item_title, listData.avatar_image_name, listData.avatar_image_url, swipeReaction));
                    count++;


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            ref1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                                        firebase_loaded = 1;


                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                }
            );
        } else {


            mData.add(new ConcreteData(0, viewType, "Phillip Melendez", R.drawable.ic_avatarcaptain, swipeReaction));
            mData.add(new ConcreteData(1, viewType, "Logan Farley", R.drawable.ic_avatardiver, swipeReaction));
            mData.add(new ConcreteData(2, viewType, "Roxanne Ramsey", R.drawable.ic_avatarastronaut, swipeReaction));
            mData.add(new ConcreteData(3, viewType, "Tierra Bryan", R.drawable.ic_avatardetective, swipeReaction));
            mData.add(new ConcreteData(4, viewType, "Allen Fletcher", R.drawable.ic_avatardisc_jockey, swipeReaction));
            mData.add(new ConcreteData(5, viewType, "William Mcbride", R.drawable.ic_avatarshowman, swipeReaction));
            mData.add(new ConcreteData(6, viewType, "Ashton Sims", R.drawable.ic_avatarcaptain, swipeReaction));
            mData.add(new ConcreteData(7, viewType, "Sonia Lyons", R.drawable.ic_avatardiver, swipeReaction));
            mData.add(new ConcreteData(8, viewType, "Tonya Avery", R.drawable.ic_avatarastronaut, swipeReaction));
            mData.add(new ConcreteData(9, viewType, "Justus Richards", R.drawable.ic_avatardetective, swipeReaction));
            mData.add(new ConcreteData(10, viewType, "Chanel Burgess", R.drawable.ic_avatardisc_jockey, swipeReaction));

        }


    }

    @Override
    public Integer getFirebase_enabled() {
        return firebase_enabled;
    }

    @Override
    public Integer getFirebase_loaded() {

        return firebase_loaded;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Data getItem(int index) {
        if (index < 0 || index >= getCount()) {
            throw new IndexOutOfBoundsException("index = " + index);
        }

        return mData.get(index);
    }

    /*@Override
    public int undoLastRemoval() {
        if (mLastRemovedData != null) {
            int insertedPosition;
            if (mLastRemovedPosition >= 0 && mLastRemovedPosition < mData.size()) {
                insertedPosition = mLastRemovedPosition;
            } else {
                insertedPosition = mData.size();
            }

            mData.add(insertedPosition, mLastRemovedData);

            mLastRemovedData = null;
            mLastRemovedPosition = -1;

            return insertedPosition;
        } else {
            return -1;
        }
    }*/

    @Override
    public void moveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        final ConcreteData item = mData.remove(fromPosition);

        mData.add(toPosition, item);
        mLastRemovedPosition = -1;
    }

    /*@Override
    public void swapItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        Collections.swap(mData, toPosition, fromPosition);
        mLastRemovedPosition = -1;
    }*/

    /*@Override
    public void removeItem(int position) {
        //noinspection UnnecessaryLocalVariable
        final ConcreteData removedItem = mData.remove(position);

        mLastRemovedData = removedItem;
        mLastRemovedPosition = position;
    }*/

    public static final class ConcreteData extends Data {

        private final long mId;
        private int mFirebase = 0;
        private final String mText;
        private String mAvatar_image_name;
        private String mAvatar_image_url;
        private Integer pic_resource;
        private final int mViewType;
        private boolean mPinned;

        ConcreteData(long id, int viewType, String text, Integer picture_resource, int swipeReaction) {
            mId = id;
            mViewType = viewType;
            pic_resource = picture_resource;

            mText = makeText(id, text, swipeReaction);

        }

        ConcreteData(long id, int viewType, String text, String avatar_image_name, String avatar_image_url, int swipeReaction) {
            mId = id;
            mViewType = viewType;
            mAvatar_image_name = avatar_image_name;
            mAvatar_image_url = avatar_image_url;


            mText = makeText(id, text, swipeReaction);
        }

        private static String makeText(long id, String text, int swipeReaction) {
            final StringBuilder sb = new StringBuilder();

          /*  sb.append(id);
            sb.append(" - ");*/
            sb.append(text);

            return sb.toString();
        }

        public int getmFirebase() {
            return mFirebase;
        }

        public void setmFirebase(int mFirebase) {
            this.mFirebase = mFirebase;
        }


       /* @Override
        public boolean isSectionHeader() {
            return false;
        }*/

        @Override
        public int getViewType() {
            return mViewType;
        }

        @Override
        public long getId() {
            return mId;
        }

        @Override
        public String toString() {
            return mText;
        }

        @Override
        public String getText() {
            return mText;
        }

        public void setAvatar_image_name(String avatar_image_name) {
            this.mAvatar_image_name = avatar_image_name;
        }

        @Override
        public String getAvatar_image_url() {
            return mAvatar_image_url;
        }

        public void setAvatar_image_url(String avatar_image_url) {
            this.mAvatar_image_url = avatar_image_url;
        }

        @Override
        public String getAvatar_image_name() {
            return mAvatar_image_name;
        }

        @Override
        public Integer getPic_resource() {
            return pic_resource;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        @Override
        public void setPinned(boolean pinned) {
            mPinned = pinned;
        }
    }

    public static final class ListData {


        String avatar_image_name;
        String avatar_image_url;
        String item_title;

        ListData() {
        }

        ListData(String avatar_image_name, String item_title, String avatar_image_url) {
            this.avatar_image_name = avatar_image_name;
            this.item_title = item_title;
            this.avatar_image_url = avatar_image_url;

        }

        public String getAvatar_image_name() {
            return avatar_image_name;
        }

        public void setAvatar_image_name(String avatar_image_name) {
            this.avatar_image_name = avatar_image_name;
        }

        public String getAvatar_image_url() {
            return avatar_image_url;
        }

        public void setAvatar_image_url(String avatar_image_url) {
            this.avatar_image_url = avatar_image_url;
        }

        public String getItem_title() {
            return item_title;
        }

        public void setItem_title(String item_title) {
            this.item_title = item_title;
        }
    }
}
