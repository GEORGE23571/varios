/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.csform.android.uiapptemplate2.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.csform.android.uiapptemplate2.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class SwipableDataProvider extends AbstractDataProviderSwipeable {
    private List<ConcreteData> mData;
    private ConcreteData mLastRemovedData;
    private int mLastRemovedPosition = -1;
    private Integer firebase_enabled = 0;
    private volatile Integer firebase_loaded;
    private Context context;
    private StorageReference mStorageRef;

    public SwipableDataProvider(Context context) {

        this.context = context;
        SharedPreferences subscribe = context.getSharedPreferences("FIREBASE_ACTIVE", MODE_PRIVATE);
        firebase_enabled = subscribe.getInt("firebase_enabled", 0);


        mData = new LinkedList<>();
        final int viewType = 0;
        final int swipeReaction = RecyclerViewSwipeManager.REACTION_CAN_SWIPE_UP | RecyclerViewSwipeManager.REACTION_CAN_SWIPE_DOWN;


        if (firebase_enabled == 1) {

            firebase_loaded = 0;
            mStorageRef = FirebaseStorage.getInstance().getReference();
            final FirebaseDatabase database1 = FirebaseDatabase.getInstance();

            DatabaseReference ref1 = database1.getReference("Lists/swipe_to_dismiss");

            ref1.addChildEventListener(new ChildEventListener() {
                int count = 0;

                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    ListData listData = dataSnapshot.getValue(ListData.class);

                    mData.add(new ConcreteData(count, viewType, listData.avatar_image_name, listData.avatar_image_url, listData.item_title, listData.item_subtitle, listData.span, swipeReaction));
                    count++;


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            ref1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                                        firebase_loaded = 1;


                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                }
            );
        } else {

            mData.add(new ConcreteData(0, viewType, "Grant Marshall", "(1999)", "marshall@yahoo.com", R.drawable.ic_avatarcaptain, swipeReaction));
            mData.add(new ConcreteData(1, viewType, "Pena Valdez", "(1980)", "valdez@yahoo.com", R.drawable.ic_avatarshowman, swipeReaction));
            mData.add(new ConcreteData(2, viewType, "Jessica Miles", "(1975)", "miles@mail.com", R.drawable.ic_avatardisc_jockey, swipeReaction));
            mData.add(new ConcreteData(3, viewType, "Kerri Barber", "(1985)", "barber@gmail.com", R.drawable.ic_avatarastronaut, swipeReaction));
            mData.add(new ConcreteData(4, viewType, "Natasha Gamble", "(1995)", "gamble@outlook.com", R.drawable.ic_avatardetective, swipeReaction));
            mData.add(new ConcreteData(5, viewType, "White Castaneda", "(1984)", "castaneda@mail.com", R.drawable.ic_avatardisc_jockey, swipeReaction));
            mData.add(new ConcreteData(6, viewType, "Vanessa Ryan", "(1960)", "ryan@mail.com", R.drawable.ic_avatardiver, swipeReaction));
            mData.add(new ConcreteData(7, viewType, "Meredith Hendricks", "(1977)", "hendricks@yahoo.com", R.drawable.ic_avatardetective, swipeReaction));
            mData.add(new ConcreteData(8, viewType, "Carol Kelly", "(1988)", "kelly@mail.com", R.drawable.ic_avatardiver, swipeReaction));
            mData.add(new ConcreteData(9, viewType, "Barrera Ramsey", "(1985)", "ramsey@gmail.com", R.drawable.ic_avatarshowman, swipeReaction));


        }

       /* for (int i = 0; i < 2; i++) {
            for (int j = 0; j < atoz.length(); j++) {
                final long id = mData.size();
                final int viewType = 0;
                final String text = Character.toString(atoz.charAt(j));
                final int swipeReaction = RecyclerViewSwipeManager.REACTION_CAN_SWIPE_UP | RecyclerViewSwipeManager.REACTION_CAN_SWIPE_DOWN;
                mData.add(new ConcreteData(id, viewType, text, R.drawable.ic_avatarsailor, swipeReaction));
            }
        }*/
    }

    @Override
    public Integer getFirebase_enabled() {
        return firebase_enabled;
    }

    @Override
    public Integer getFirebase_loaded() {

        return firebase_loaded;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Data getItem(int index) {


        if (index < 0 || index == getCount()) {
            return null;
        }

        if (index < 0 || index >= getCount()) {
            throw new IndexOutOfBoundsException("index = " + index);
        }

        return mData.get(index);
    }


    @Override
    public int undoLastRemoval() {
        if (mLastRemovedData != null) {
            int insertedPosition;
            if (mLastRemovedPosition >= 0 && mLastRemovedPosition < mData.size()) {
                insertedPosition = mLastRemovedPosition;
            } else {
                insertedPosition = mData.size();
            }

            mData.add(insertedPosition, mLastRemovedData);

            mLastRemovedData = null;
            mLastRemovedPosition = -1;

            return insertedPosition;
        } else {
            return -1;
        }
    }

    @Override
    public void moveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        final ConcreteData item = mData.remove(fromPosition);

        mData.add(toPosition, item);
        mLastRemovedPosition = -1;
    }

    @Override
    public void swapItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        Collections.swap(mData, toPosition, fromPosition);
        mLastRemovedPosition = -1;
    }

    @Override
    public void removeItem(int position) {
        //noinspection UnnecessaryLocalVariable
        final ConcreteData removedItem = mData.remove(position);

        mLastRemovedData = removedItem;
        mLastRemovedPosition = position;
    }

    public static final class ConcreteData extends Data {

        private final long mId;
        private String mTextName;
        private String mTextYear;
        private String mTextEmail;
        private String picture_link;
        private String mAvatar_image_url;
        private Integer pic_resource;
        private final int mViewType;
        private boolean mPinned;

        ConcreteData(long id, int viewType, String textName, String textYear, String textEmail, Integer picture_resource, int swipeReaction) {
            mId = id;
            mViewType = viewType;
            pic_resource = picture_resource;
            mTextYear = makeText(id, textYear, swipeReaction);
            mTextName = textName;
            mTextEmail = textEmail;

        }

        ConcreteData(long id, int viewType, String text, String picture_link, int swipeReaction) {
            mId = id;
            mViewType = viewType;
            picture_link = picture_link;
            mTextYear = makeText(id, text, swipeReaction);
        }

        ConcreteData(long id, int viewType, String avatar_image_name, String avatar_image_url, String item_title, String item_subtitle, String span, int swipeReaction) {
            mId = id;
            mViewType = viewType;
            picture_link = avatar_image_name;
            mAvatar_image_url = avatar_image_url;
            mTextYear = makeText(id, span, swipeReaction);
            mTextName = item_title;
            mTextEmail = item_subtitle;

        }


        private static String makeText(long id, String text, int swipeReaction) {
            final StringBuilder sb = new StringBuilder();

          /*  sb.append(id);
            sb.append(" - ");*/
            sb.append(text);

            return sb.toString();
        }

        @Override
        public boolean isSectionHeader() {
            return false;
        }

        @Override
        public int getViewType() {
            return mViewType;
        }

        @Override
        public long getId() {
            return mId;
        }


        @Override
        public String toString() {
            return mTextName;
        }

        @Override
        public String getTextName() {
            return mTextName;
        }

        @Override
        public String getTextYear() {
            return mTextYear;
        }

        @Override
        public String getTextEmail() {
            return mTextEmail;
        }

        @Override
        public String getPicture_link() {
            return picture_link;
        }

        @Override
        public String getAvatar_image_url() {
            return mAvatar_image_url;
        }


        @Override
        public Integer getPic_resource() {
            return pic_resource;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        @Override
        public void setPinned(boolean pinned) {
            mPinned = pinned;
        }
    }

    public static final class ListData {


        String avatar_image_name;
        String avatar_image_url;
        String item_title;
        String item_subtitle;
        String span;

        ListData() {
        }

        ListData(String avatar_image_name, String avatar_image_url, String item_title, String item_subtitle, String span) {
            this.avatar_image_name = avatar_image_name;
            this.avatar_image_url = avatar_image_url;
            this.item_title = item_title;
            this.item_subtitle = item_subtitle;
            this.span = span;

        }

        public String getAvatar_image_name() {
            return avatar_image_name;
        }

        public void setAvatar_image_name(String avatar_image_name) {
            this.avatar_image_name = avatar_image_name;
        }

        public String getAvatar_image_url() {
            return avatar_image_url;
        }

        public void setAvatar_image_url(String avatar_image_url) {
            this.avatar_image_url = avatar_image_url;
        }

        public String getSpan() {
            return span;
        }

        public void setSpan(String span) {
            this.span = span;
        }

        public String getItem_subtitle() {
            return item_subtitle;
        }

        public void setItem_subtitle(String item_subtitle) {
            this.item_subtitle = item_subtitle;
        }

        public String getItem_title() {
            return item_title;
        }

        public void setItem_title(String item_title) {
            this.item_title = item_title;
        }
    }

}
