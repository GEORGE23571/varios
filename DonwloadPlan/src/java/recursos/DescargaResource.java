package recursos;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import servicios.Actualizacion;
import servicios.Archivo;


@Path("/")
public class DescargaResource 
{

    @Context
    private UriInfo context;
    
    public DescargaResource() {
    }


    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getPlan(  @Context HttpServletRequest requestContext,@Context SecurityContext context       ) throws IOException 
    {
        boolean result = Actualizacion.sum(requestContext.getRemoteAddr());
         if(!result)
         {
              return Response.serverError()
              .status(Response.Status.CONFLICT)
              .entity(Actualizacion.getError() )
              .build();
         }
         else
         {
            return Response.ok(Archivo.getFile(), MediaType.APPLICATION_OCTET_STREAM)
            .header("Content-Disposition", "attachment; filename=\"plan_multianual.exe\"" )
            .build();
         }
       
    }
    
    @GET
    public Response getUpdateMapa1(  @Context HttpServletRequest requestContext,@Context SecurityContext context       ) throws IOException 
    {
        Actualizacion.mapa1(requestContext.getRemoteAddr());
        return Response.ok().build();
    }
}
