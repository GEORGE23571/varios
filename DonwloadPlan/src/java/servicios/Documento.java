
package servicios;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Documento
{
    private static String error;
    
    public static   String escribir(String cont,String path)
    {
        BufferedWriter bw = null;
        FileWriter fw = null;
        try 
        {
            //C:\Users\Bayron Lopez\Desktop
            //File file = new File("/home/ubuntu/contador.txt");
            File file = new File(path);
            // Si el archivo no existe, se crea!
            if (!file.exists()) {
                file.createNewFile();
            }
            // flag true, indica adjuntar información al archivo.
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(cont+"\n");

        } catch (IOException e)
        {
            error=e.getMessage();
            return null;
        } finally 
        {
            try {
                            //Cierra instancias de FileWriter y BufferedWriter
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) 
            {
                error=ex.getMessage();
                return null;
            }
        }
        
        return "";
    }
    
    public static String muestraContenido( String ruta) 
    {
        String cadena;
        String result="";
        FileReader f;
        try {
            f = new FileReader(ruta);
            BufferedReader b = new BufferedReader(f);
        
            while((cadena = b.readLine())!=null)
            {
                System.out.println(cadena);
                result=cadena;
            }
            b.close();
        } catch (FileNotFoundException ex) 
        {
             error= ex.getMessage();
             return null;
        }
        catch (IOException ex) 
        {
            error= ex.getMessage();
            return null;
        }
        
        return result;
    }

    public static String getError() {
        return error;
    }

    public static void setError(String aError) {
        error = aError;
    }
    
    
    
}
