﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Vista;

namespace WindowsFormsApp1
{
    public partial class Ajuste : MetroForm
    {
        int year;
        Elemento nodo;
        int porcentaje_equitativo;
        public Ajuste()
        {
            InitializeComponent();
            this.label11.Text = "Estimación de metas para las \n estrategias seleccionadas.";
        }

        public void set_year(int year)
        {
            this.year = year;
            label1.Text = "" + year++;
            label2.Text = "" + year++;
            label3.Text = "" + year++;
            label4.Text = "" + year++;
            label5.Text = "" + year++;
            label6.Text = "" + year++;
            label7.Text = "" + year++;
            label8.Text = "" + year++;
            label9.Text = "" + year++;
            label10.Text = "" + year++;
            year = year - 10;
        }

        public void Valores_iniciales(Elemento e)
        {
            trackBar1.Value = (int)e.Porcentajes[0];
            trackBar2.Value = (int)e.Porcentajes[1];
            trackBar3.Value = (int)e.Porcentajes[2];
            trackBar4.Value = (int)e.Porcentajes[3];
            trackBar5.Value = (int)e.Porcentajes[4];
            trackBar6.Value = (int)e.Porcentajes[5];
            trackBar7.Value = (int)e.Porcentajes[6];
            trackBar8.Value = (int)e.Porcentajes[7];
            trackBar9.Value = (int)e.Porcentajes[8];
            trackBar10.Value = (int)e.Porcentajes[9];
            this.nodo = e;
        }

        public void ajustar(int i)
        {
            //verificar si suma mas de 100
            int suma = 0;
            suma += trackBar1.Value;
            suma += trackBar2.Value;
            suma += trackBar3.Value;
            suma += trackBar4.Value;
            suma += trackBar5.Value;
            suma += trackBar6.Value;
            suma += trackBar7.Value;
            suma += trackBar8.Value;
            suma += trackBar9.Value;
            suma += trackBar10.Value;
            if (suma>100)
            {
                switch (i)
                {
                    case 1:
                        trackBar1.Value =trackBar1.Value-1;
                        break;
                    case 2:
                        trackBar2.Value = trackBar2.Value - 1;
                        break;
                    case 3:
                        trackBar3.Value = trackBar3.Value - 1;
                        break;
                    case 4:
                        trackBar4.Value = trackBar4.Value - 1;
                        break;
                    case 5:
                        trackBar5.Value = trackBar5.Value - 1;
                        break;
                    case 6:
                        trackBar6.Value = trackBar6.Value - 1;
                        break;
                    case 7:
                        trackBar7.Value = trackBar7.Value - 1;
                        break;
                    case 8:
                        trackBar8.Value = trackBar8.Value - 1;
                        break;

                    case 9:
                        trackBar9.Value = trackBar9.Value - 1;
                        break;
                    case 10:
                        trackBar10.Value = trackBar10.Value - 1;
                        break;
                }
            }
            //poner suma parcial de los trackbar
            suma = trackBar1.Value;
            suma += trackBar2.Value;
            suma += trackBar3.Value;
            suma += trackBar4.Value;
            suma += trackBar5.Value;
            suma += trackBar6.Value;
            suma += trackBar7.Value;
            suma += trackBar8.Value;
            suma += trackBar9.Value;
            suma += trackBar10.Value;
            metroLabel1.Text = suma + " %";
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            trackBar1.Value = (int)numericUpDown1.Value;
            ajustar(1);
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown1.Value = trackBar1.Value;
          
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            trackBar2.Value = (int)numericUpDown2.Value;
        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown2.Value = trackBar2.Value;
            ajustar(2);
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            trackBar3.Value = (int)numericUpDown3.Value;
        }

        private void trackBar3_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown3.Value = trackBar3.Value;
            ajustar(3);
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            trackBar4.Value = (int)numericUpDown4.Value;
        }

        private void trackBar4_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown4.Value = trackBar4.Value;
            ajustar(4);
        }
        
        private void trackBar5_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown5.Value = trackBar5.Value;
        }

        private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        {
            trackBar5.Value = (int)numericUpDown5.Value;
            ajustar(5);
        }
        private void numericUpDown6_ValueChanged(object sender, EventArgs e)
        {
            trackBar6.Value = (int)numericUpDown6.Value;
        }

        private void trackBar6_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown6.Value = trackBar6.Value;
            ajustar(6);
        }

        private void numericUpDown7_ValueChanged(object sender, EventArgs e)
        {
            trackBar7.Value = (int)numericUpDown7.Value;
        }

        private void trackBar7_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown7.Value = trackBar7.Value;
            ajustar(7);
        }

        private void numericUpDown8_ValueChanged(object sender, EventArgs e)
        {
            trackBar8.Value = (int)numericUpDown8.Value;
        }

        private void trackBar8_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown8.Value = trackBar8.Value;
            ajustar(8);
        }

        private void numericUpDown9_ValueChanged(object sender, EventArgs e)
        {
            trackBar9.Value = (int)numericUpDown9.Value;
        }

        private void trackBar9_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown9.Value = trackBar9.Value;
            ajustar(9);
        }

        private void numericUpDown10_ValueChanged(object sender, EventArgs e)
        {
            trackBar10.Value = (int)numericUpDown10.Value;
        }

        private void trackBar10_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown10.Value = trackBar10.Value;
            ajustar(10);
        }

        private void Ajuste_Deactivate(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int suma = trackBar1.Value;
            suma += trackBar2.Value;
            suma += trackBar3.Value;
            suma += trackBar4.Value;
            suma += trackBar5.Value;
            suma += trackBar6.Value;
            suma += trackBar7.Value;
            suma += trackBar8.Value;
            suma += trackBar9.Value;
            suma += trackBar10.Value;
            if (suma < 100)
            {
                Mensaje m = new Mensaje();
                m.mensaje("Debe ajustarse el 100%");
                m.ShowDialog();
            }
            else
            {
                nodo.Porcentajes[0] = trackBar1.Value;
                nodo.Porcentajes[1] = trackBar2.Value;
                nodo.Porcentajes[2] = trackBar3.Value;
                nodo.Porcentajes[3] = trackBar4.Value;
                nodo.Porcentajes[4] = trackBar5.Value;
                nodo.Porcentajes[5] = trackBar6.Value;
                nodo.Porcentajes[6] = trackBar7.Value;
                nodo.Porcentajes[7] = trackBar8.Value;
                nodo.Porcentajes[8] = trackBar9.Value;
                nodo.Porcentajes[9] = trackBar10.Value;
                this.Close();
            }
            
            
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            trackBar1.Value = 0;
            trackBar2.Value = 0;
            trackBar3.Value = 0;
            trackBar4.Value = 0;
            trackBar5.Value = 0;
            trackBar6.Value = 0;
            trackBar7.Value = 0;
            trackBar8.Value = 0;
            trackBar9.Value = 0;
            trackBar10.Value = 0;
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            trackBar1.Value = porcentaje_equitativo;
            trackBar2.Value = porcentaje_equitativo;
            trackBar3.Value = porcentaje_equitativo;
            trackBar4.Value = porcentaje_equitativo;
            trackBar5.Value = porcentaje_equitativo;
            trackBar6.Value = porcentaje_equitativo;
            trackBar7.Value = 10;
            trackBar8.Value = 10;
            trackBar9.Value = 10;
            trackBar10.Value = 10;

            switch (nodo.anios + 1)
            {
                case 10:
                    trackBar10.Value = porcentaje_equitativo;
                    goto case 9;
                case 9:
                    trackBar9.Value = porcentaje_equitativo;
                    goto case 8;
                case 8:
                    trackBar8.Value = porcentaje_equitativo;
                    goto case 7;
                case 7:
                    trackBar7.Value = porcentaje_equitativo;
                    goto case 6;
                case 6:
                    trackBar6.Value = porcentaje_equitativo;
                    goto case 5;
                case 5:
                    trackBar5.Value = porcentaje_equitativo;
                    goto case 4;
                case 4:
                    trackBar4.Value = porcentaje_equitativo;
                    goto case 3;
                case 3:
                    trackBar3.Value = porcentaje_equitativo;
                    goto case 2;
                case 2:
                    trackBar2.Value = porcentaje_equitativo;
                    goto case 1;
                case 1:
                    trackBar1.Value = porcentaje_equitativo;
                    break;
            }
            
        }

        private void Ajuste_Load(object sender, EventArgs e)
        {
            //porcentaje equitativo
            porcentaje_equitativo = (100 / nodo.anios);
            this.metroButton2.Text = this.porcentaje_equitativo+"%";

            //bloqueo de controles para anios no disponibles
            switch (nodo.anios+1)
            {
                case 1:
                    trackBar1.Enabled = false;
                    numericUpDown1.Enabled = false;
                    goto case 2;
                case 2:
                    trackBar2.Enabled = false;
                    numericUpDown2.Enabled = false;
                    goto case 3;
                case 3:
                    trackBar3.Enabled = false;
                    numericUpDown3.Enabled = false;
                    goto case 4;
                case 4:
                    trackBar4.Enabled = false;
                    numericUpDown4.Enabled = false;
                    goto case 5;
                case 5:
                    trackBar5.Enabled = false;
                    numericUpDown5.Enabled = false;
                    goto case 6;
                case 6:
                    trackBar6.Enabled = false;
                    numericUpDown6.Enabled = false;
                    goto case 7;
                case 7:
                    trackBar7.Enabled = false;
                    numericUpDown7.Enabled = false;
                    goto case 8;
                case 8:
                    trackBar8.Enabled = false;
                    numericUpDown8.Enabled = false;
                    goto case 9;
                case 9:
                    trackBar9.Enabled = false;
                    numericUpDown9.Enabled = false;
                    goto case 10;
                case 10:
                    trackBar10.Enabled = false;
                    numericUpDown10.Enabled = false;
                    break;
            }
        }
    }
}
