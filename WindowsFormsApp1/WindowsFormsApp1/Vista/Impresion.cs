﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Impresion : Form
    {
        DataGridView grid;
        PrintDocument doc = new PrintDocument();
        public Impresion()
        {
            InitializeComponent();
        }

        public void set_grid(DataGridView g)
        {

            this.grid = g;
            imprimir();
            printPreviewControl1.Document = doc;

        }


        private void imprimir ()
       {
            doc.DefaultPageSettings.Landscape = false;

            int filas = 0;
            doc.PrintPage += delegate (object ev, PrintPageEventArgs ep)
            {
                ep.HasMorePages = true;
                const int DGV_ALTO = 35;
                int left = ep.MarginBounds.Left, top = ep.MarginBounds.Top;

                for (int j = filas; j < grid.RowCount; j++)
                {
                    left = ep.MarginBounds.Left;
                    foreach (DataGridViewCell cell in grid.Rows[j].Cells)
                    {
                        ep.Graphics.DrawString(Convert.ToString(cell.Value), new Font("Segoe UI", 13), Brushes.Black, left, top + 4);
                        left += cell.OwningColumn.Width;
                    }
                    top += DGV_ALTO;
                    ep.Graphics.DrawLine(Pens.Gray, ep.MarginBounds.Left, top, ep.MarginBounds.Right, top);


                    if (j - filas > 21)
                    {
                        filas += 21;
                        ep.HasMorePages = true;
                        return;
                    }
                    else
                    {
                        ep.HasMorePages = false;
                    }

                }

            };

        }
        

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
           
        }
        
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            int star_page = printPreviewControl1.StartPage;
            doc.DefaultPageSettings.Landscape = true ;
            printPreviewControl1.Document = doc;
            printPreviewControl1.Refresh();
            printPreviewControl1.StartPage = star_page;
        }

        private void printPreviewControl1_Click(object sender, EventArgs e)
        {
            printPreviewControl1.StartPage = printPreviewControl1.StartPage + 1;
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            printPreviewControl1.StartPage = 5;
            
            printPreviewControl1.Refresh();
        }
    }
}
