﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Vista
{
    public partial class Mensaje : MetroForm
    {
        public Mensaje()
        {
            InitializeComponent();
        }
        public void mensaje(string mensaje)
        {
            this.metroLabel1.Text = mensaje;
        }
        private void metroButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
