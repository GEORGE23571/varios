﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class trackbar_ODA :MetroForm
    {
        Elemento nodo;
        public trackbar_ODA()
        {
            InitializeComponent();
        }
        public void set_valor(Elemento e)
        {
            trackBar1.Value = e.Porcentaje;
            numericUpDown2.Value = e.anios;
            this.nodo = e;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            trackBar1.Value = (int)numericUpDown1.Value;
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown1.Value = trackBar1.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            nodo.Porcentaje = trackBar1.Value;
            
            nodo.set_anios_trabajo((int)numericUpDown2.Value);
            this.Close();
        }
        
    }
}
