﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WindowsFormsApp1.Modelo;
using MetroFramework.Forms;
using WindowsFormsApp1.Vista;
using System.Reflection;
using System.IO;
using WindowsFormsApp1.Controlador;

namespace WindowsFormsApp1
{
    public partial class Form1 : MetroForm
    {
        private Arbol Por_defecto;//arbol que sirve de referencia para cambios de la carga inicial
        private Arbol forma;//arbol de forma
        private Arbol arbol;//arbol de planificacion
        private Arbol arbol_personalizable;
        int year = 2020;//anio de inicio
        private Stage ws_inicial;//workspace por default 
        private bool seleccion_ascentente = false;
        public Form1()
        {
            InitializeComponent();
               
            //creacion del workspace incial
            resize();//metodo que redimensiona controles al tamano de la ventana
            this.ws_inicial = new Stage();//creacion del workspace inicial
            this.Por_defecto = this.ws_inicial.arbol_forma.Clone();
            dataGridView1.BringToFront();//llevar controles al frente para ocultar layuot imprevisto
            dataGridView2.BringToFront();
            dataGridView3.BringToFront();
            dataGridView4.BringToFront();
            treeView1.BringToFront();//manejo del render en los datagridview
            typeof(DataGridView).InvokeMember("DoubleBuffered",
            BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
            null, this.dataGridView1, new object[] { true });
            typeof(DataGridView).InvokeMember("DoubleBuffered",
           BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
           null, this.dataGridView2, new object[] { true });
            typeof(DataGridView).InvokeMember("DoubleBuffered",
           BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
           null, this.dataGridView3, new object[] { true });
            typeof(DataGridView).InvokeMember("DoubleBuffered",
           BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
           null, this.dataGridView4, new object[] { true });
            typeof(DataGridView).InvokeMember("DoubleBuffered",
           BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
           null, this.dataGridView5, new object[] { true });

            typeof(TreeView).InvokeMember("DoubleBuffered",
           BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
           null, this.treeView1, new object[] { true });
           // this.WindowState = FormWindowState.Maximized;// iniciar con la ventana maximizada 
        }

        private void Form1_Load(object sender, EventArgs e)
        {//crear tablas
            Tablas_Datagrid.crear_tablas(dataGridView1, dataGridView2, dataGridView3, dataGridView4, dataGridView5);
            cargar_stage(this.ws_inicial);//carga del worskspace inicial 
            refresh_label1();
        }

        private void cargar_stage(Stage stage)
        {
            this.forma = stage.arbol_forma;
            this.arbol = stage.arbol_planificacion;
            this.arbol_personalizable = stage.arbol_personalizable;
            tabControl1.SelectedIndex = 0;//tab incial  
            Tablas_Datagrid.cargar_stage(stage);

            treeView1.Nodes.Clear();
            treeView1.Nodes.Add(arbol.TreeView());
            treeView1.Nodes[0].Text = "Seleccionar todos";
            //treeView1.Nodes[0].ForeColor = Color.White;
            treeView1.Nodes[0].Expand();
            
            refresh_label1();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            resize(); 
        }

        private void resize()
        {
            pictureBox2.SetBounds((3 * this.Width / 5) - 30, 20, 500, 50);
            pictureBox2.BackColor = Color.White;
            tabControl1.SetBounds(30, 110, this.Width - 60, this.Height - 140);
            panel1.SetBounds(0, tabControl1.Bottom + 1, this.Width, 140);
            panel1.BackColor = Color.FromArgb(207, 207, 207);

            label1.SetBounds(5 * this.Width / 8, 0, label1.Parent.Height, 100);

            label1.Text = "";
            // tabControl1.SetBounds(200, 115, this.Width - 40, this.Height - 165);
        }


        private void treeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            Elemento nodo_asociado;//objecto asociado en el arbol, del nodo treeview seleccionado
            nodo_asociado = arbol.Find_node_elemento(e.Node);//Nodo asociado

            if (!e.Node.Checked) //quitar seleccion
            {
                if (nodo_asociado.Id == 0 && seleccion_ascentente)
                {
                        return;
                }
                if (nodo_asociado.opcional == 1 && e.Node.Parent.Checked)//no es opcional y su padre esta seleccionado
                {
                    e.Node.Checked = true;
                    this.metroToolTip1.IsBalloon = true;
                    this.metroToolTip1.SetToolTip(this.treeView1, "No es opcional");
                    
                }
                else
                {
                    if (nodo_asociado.Id != 0)//expandir el primer nivel
                    {
                        e.Node.Collapse();
                    }

                    foreach (TreeNode n in e.Node.Nodes)//desmarcar a hijos no importando si son opcionales
                    {
                        n.Checked = false;//deseleccionar
                    }
                }

                if (e.Node.TreeView.Nodes[0].Checked)//se encuentra seleccionado el nodo raiz
                {
                    e.Node.TreeView.Nodes[0].Text = "Seleccionar todo";
                    this.seleccion_ascentente = true;
                    e.Node.TreeView.Nodes[0].Checked = false;
                    this.seleccion_ascentente = false;
                }
                if (!e.Node.Checked)
                {
                    e.Node.TreeView.Nodes[0].Text = "Seleccionar todo";
                }
                
                if (nodo_asociado.opcional == 1)
                {
                    e.Node.ForeColor = Color.Gray;
                }
            }
            else
            {//Seleccionar
                e.Node.Expand();//expande su nivel inmediato
                                //si el nodo que se selecciono contiene opciones , se evalua si otro de la opciones esta seleccionada

                if (nodo_asociado.Unique_selection.Count > 0 && nodo_asociado.Id != 0)
                {
                    foreach (int i in nodo_asociado.Unique_selection)
                    {
                        TreeNode node_tv = arbol.Find_node_tv(i, e.Node);
                        if (node_tv.Checked)
                        {
                            node_tv.ForeColor = Color.Red;
                            Mensaje m = new Mensaje();
                            m.ShowDialog();
                            node_tv.ForeColor = Color.Black;
                            e.Node.Checked = false;
                            e.Node.Collapse();
                            //como máximo 1 debe estar seleccionado
                            return;
                        }
                    }
                }

                if (nodo_asociado.Id == 0)//si es la raiz, cambiar el nombre a deseleccionar
                {
                    e.Node.Text = "Deseleccionar todo";
                }

                for (int i = 0; i < nodo_asociado.hijos.Count; i++)//seleccionar siempre a los hijos que no tengan opciones
                {
                    if (seleccion_ascentente)
                    {
                        if (nodo_asociado.hijos[i].Unique_selection.Count == 0 && nodo_asociado.hijos[i].opcional == 1)//Si no tiene opciones, marcar
                        {
                            e.Node.Nodes[i].Checked = true;
                        }
                        else if (nodo_asociado.hijos[i].seleccion_default && nodo_asociado.hijos[i].opcional == 1)//indica que es la opcion por default y permite seleccionarlo 
                        {
                            e.Node.Nodes[i].Checked = true;
                        }
                    }
                    else
                    {
                        if (nodo_asociado.hijos[i].Unique_selection.Count == 0)//Si no tiene opciones, marcar
                        {
                            e.Node.Nodes[i].Checked = true;
                        }
                        else if (nodo_asociado.hijos[i].seleccion_default)//indica que es la opcion por default y permite seleccionarlo 
                        {
                            e.Node.Nodes[i].Checked = true;
                        }
                    }
                }

                if (e.Node.Parent != null &&
               (!e.Node.Parent.Equals(e.Node.TreeView.Nodes[0])) &&
               !e.Node.Parent.Checked)//si su padre es distinto de la raiz, seleccionarlo si aun no se encuentra seleccionado
                {
                    if (!seleccion_ascentente)
                    {
                        seleccion_ascentente = true;
                        e.Node.Parent.Checked = true;
                        seleccion_ascentente = false;

                    }
                    else
                    {
                        e.Node.Parent.Checked = true;
                    }
                }
                if (nodo_asociado.opcional==1)
                {
                    e.Node.ForeColor = Color.Gray;
                }
            }
            refresh_label1();
        }

        private void refresh_label1()
        {
            arbol.marcar(treeView1.Nodes[0]);
            arbol.Totalizar();
            if (arbol.Raiz.Total == 0)
            {
                label1.Text = "";
                toolStripDropDownButton2.Enabled = false;
            }
            else
            {
                label1.Text = "Costo:  " + String.Format("{0:#,##0.00}", arbol.Raiz.Total);
                toolStripDropDownButton2.Enabled = true;
            }
        }

        private void dataGridView3_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex==-1)
            {
                trackbar_ODA a = new trackbar_ODA();
                a.set_valor(arbol_personalizable.Raiz);
                a.StartPosition = FormStartPosition.CenterParent;
                a.ShowDialog(this);
                arbol_personalizable.Raiz.HeredarPorcentaje();
            }
            else
            {
                int clave = Int32.Parse(dataGridView3.Rows[e.RowIndex].Cells[0].Value.ToString());
                foreach (Elemento nodo in arbol_personalizable.lista)//lo busco en la lista de nodos chekeados y aplico método                       hacer busqueda por id, ya que esta indexado naturamente
                {
                    if (clave != 1000 && nodo.Id == clave && nodo.Nivel == 1)
                    {
                        if (dataGridView3.CurrentCell.ColumnIndex == 3)
                        {

                            trackbar_ODA a = new trackbar_ODA();
                            a.set_valor(nodo);
                            a.StartPosition = FormStartPosition.CenterParent;
                            a.ShowDialog(this);
                        }
                        else
                        {
                            Ajuste a = new Ajuste();
                            a.set_year(year);
                            a.Valores_iniciales(nodo);
                            a.StartPosition = FormStartPosition.CenterParent;
                            a.ShowDialog(this);
                        }
                        nodo.HeredarPorcentaje();
                    }
                }
            }

            arbol_personalizable.Totalizar();
            Tablas_Datagrid.BindData_personalizable();
            Tablas_Datagrid.pintar_suma(dataGridView3, metroTabPage4);
            if (e.RowIndex != -1)
                dataGridView3.CurrentCell = dataGridView3.Rows[e.RowIndex].Cells[e.ColumnIndex];
            dataGridView3.BeginEdit(true);
            label1.Text = "Costo Multianual:  "+ String.Format("{0:#,##0.00}", arbol_personalizable.Raiz.Total);
            
        }

        private void imprimir(DataGridView dg)
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "XLS files (*.xls)|*.xls|XLT files (*.xlt)|*.xlt|XLSX files (*.xlsx)|*.xlsx|XLSM files (*.xlsm)|*.xlsm|XLTX (*.xltx)|*.xltx|XLTM (*.xltm)|*.xltm|ODS (*.ods)|*.ods|OTS (*.ots)|*.ots|CSV (*.csv)|*.csv|TSV (*.tsv)|*.tsv|HTML (*.html)|*.html|MHTML (.mhtml)|*.mhtml|PDF (*.pdf)|*.pdf|XPS (*.xps)|*.xps|BMP (*.bmp)|*.bmp|GIF (*.gif)|*.gif|JPEG (*.jpg)|*.jpg|PNG (*.png)|*.png|TIFF (*.tif)|*.tif|WMP (*.wdp)|*.wdp";
            saveFileDialog.FilterIndex = 3;

            if (saveFileDialog.ShowDialog() != DialogResult.OK)
            {return;}

            Microsoft.Office.Interop.Excel.Application excel;
            Microsoft.Office.Interop.Excel.Workbook worKbooK;
            Microsoft.Office.Interop.Excel.Worksheet worKsheeT;
            Microsoft.Office.Interop.Excel.Range celLrangE;
            //crear progressbar
            PBar avance = new PBar();
            avance.Show();
            Application.DoEvents();
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.Visible = false;
                excel.DisplayAlerts = false;
                worKbooK = excel.Workbooks.Add(Type.Missing);
                worKsheeT = (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.ActiveSheet;
                int j = 1;
                foreach (DataGridViewColumn dc in dg.Columns)
                {
                    if (!dc.Visible)
                    {
                        continue;
                    }
                    worKsheeT.Cells[1, j] = dc.HeaderText.ToString();
                    j++;
                }//color de cabecera
                celLrangE = worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[1, j - 1]];
                celLrangE.Interior.Color = dg.ColumnHeadersDefaultCellStyle.BackColor;
                celLrangE.Font.Color = Color.White;
              
                int rowcount = 2;
                int col = 1;
                foreach (DataGridViewRow datarow in dg.Rows)
                {
                    celLrangE = worKsheeT.Range[worKsheeT.Cells[rowcount, 1], worKsheeT.Cells[rowcount, j - 1]];
                    celLrangE.Interior.Color = datarow.DefaultCellStyle.BackColor;
                    celLrangE.Font.Color = datarow.DefaultCellStyle.ForeColor;
                    for (int i = 0; i < dg.Columns.Count; i++)
                    {
                        if (dg.Columns[i].Visible)
                        {
                            worKsheeT.Cells[rowcount, col] = datarow.Cells[i].Value.ToString();
                            col++;
                        }
                    }
                    col = 1;
                    rowcount++;
                    avance.set_value((rowcount*100)/dg.RowCount);
                    Application.DoEvents();
                }

                celLrangE = worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[rowcount-1, j-1]];
                celLrangE.Font.Bold = true;
                Microsoft.Office.Interop.Excel.Borders border = celLrangE.Borders;
                border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                border.Weight = 2d;
                celLrangE.EntireColumn.AutoFit();
                celLrangE.EntireRow.AutoFit();
                celLrangE = worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[2, j-1]];
                Console.WriteLine(saveFileDialog.FileName.ToString());
                worKbooK.SaveAs(saveFileDialog.FileName.ToString());
                worKbooK.Close();
                excel.Quit();
                avance.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                worKsheeT = null;
                celLrangE = null;
                worKbooK = null;
            }
        }

        private void treeView1_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            Rectangle nodeRect = e.Node.Bounds;
            /*--------- 1. draw expand/collapse icon ---------*/
            Point ptExpand = new Point(nodeRect.Location.X - 20, nodeRect.Location.Y + 2);
            Image expandImg = null;
            if (e.Node.IsExpanded || e.Node.Nodes.Count < 1)
                expandImg = Image.FromFile(@"C:\Users\GEORGE\Downloads\minus.png");
            else
                expandImg = Image.FromFile(@"C:\Users\GEORGE\Downloads\plus.png");
            Graphics g = Graphics.FromImage(expandImg);
            IntPtr imgPtr = g.GetHdc();
            g.ReleaseHdc();
            e.Graphics.DrawImage(expandImg, ptExpand);
            /*--------- 2. draw node icon ---------*/
            //Point ptNodeIcon = new Point(nodeRect.Location.X - 4, nodeRect.Location.Y + 2);
            //Image nodeImg = Image.FromFile(@"C:\Users\GEORGE\Downloads\expand.png");
            //g = Graphics.FromImage(nodeImg);
            //imgPtr = g.GetHdc();
            //g.ReleaseHdc();
            //e.Graphics.DrawImage(nodeImg, ptNodeIcon);
            /*--------- 3. draw node text ---------*/
            //Font nodeFont = e.Node.NodeFont;
            //if (nodeFont == null)
            //    nodeFont = ((TreeView)sender).Font;
            //Brush textBrush = SystemBrushes.WindowText;
            ////to highlight the text when selected
            //if ((e.State & TreeNodeStates.Focused) != 0)
            //    textBrush = SystemBrushes.HotTrack;
            ////Inflate to not be cut
            //Rectangle textRect = nodeRect;
            ////need to extend node rect
            //textRect.Width += 40;
            //e.Graphics.DrawString(e.Node.Text, nodeFont, textBrush, Rectangle.Inflate(textRect, -12, 0));
        }

        private void metroTabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = (sender as TabControl).SelectedIndex;
            switch (i)
            {
                case 0:
                    label1.Text = "";
                    break;
                case 1://poner el porcentaje del arbol en 100 en el primer anio
                    arbol.Raiz.Porcentajes[0] = 100;
                    for (int j = 1; j < 10; j++)
                        arbol.Raiz.Porcentajes[j] = 0;
                    arbol.Raiz.set_anios_trabajo(1);
                    arbol.Raiz.HeredarPorcentaje();
                    refresh_label1();
                    break;
                case 2://mostrar solo seleccionados PLAN A DIEZ AÑOS
                    for (int j = 0; j < 10; j++)
                        arbol.Raiz.Porcentajes[j] = 10;
                    arbol.Raiz.set_anios_trabajo(10);
                    arbol.Raiz.HeredarPorcentaje();
                    arbol.Totalizar();
                    Tablas_Datagrid.BindData1();
                    Tablas_Datagrid.pintar_suma(dataGridView1, metroTabPage3);
                    label1.Text = "Costo a 10 años:  " + String.Format("{0:#,##0.00}", arbol.Raiz.Total);
                    break;
                case 3://construir sobre arbol copiado  PERSONALIZABLE
                    arbol_personalizable.marcar(treeView1.Nodes[0]);
                    arbol_personalizable.Totalizar();
                    Tablas_Datagrid.BindData_personalizable();
                    Tablas_Datagrid.pintar_suma(dataGridView3, metroTabPage4);
                    label1.Text = "Costo Multianual:  " + String.Format("{0:#,##0.00}", arbol_personalizable.Raiz.Total);
                    break;
                case 4://copiar arbol para renglones     RENGLONES
                    arbol_personalizable.marcar(treeView1.Nodes[0]);
                    arbol_personalizable.Totalizar();
                    Tablas_Datagrid.BindData_Renglon(arbol_personalizable.Arbol_Rengloneado());
                    Tablas_Datagrid.pintar_suma(dataGridView4, metroTabPage5);
                    label1.Text = "Costo :  " + String.Format("{0:#,##0.00}", arbol_personalizable.Raiz.Total);
                    break;
            }
        }

        private void dToolStripMenuItem_Click(object sender, EventArgs e)
        {//planificacion a 10 anios
            
                for (int j = 0; j < 10; j++)
                    arbol.Raiz.Porcentajes[j] = 0;
                arbol.Raiz.HeredarPorcentaje();
                tabControl1.SelectedIndex = 2;//tab incial
                Tablas_Datagrid.BindData1();
                Tablas_Datagrid.pintar_suma(dataGridView1, metroTabPage3);
            
            imprimir(dataGridView1);
        }

        private void sToolStripMenuItem_Click(object sender, EventArgs e)
        {//personalizable
          
                tabControl1.SelectedIndex = 3;//tab incial
                arbol_personalizable.marcar(treeView1.Nodes[0]);
                arbol_personalizable.Totalizar();
                Tablas_Datagrid.BindData_personalizable();
                Tablas_Datagrid.pintar_suma(dataGridView3, metroTabPage4);
            imprimir(dataGridView3);
        }

        private void sToolStripMenuItem1_Click(object sender, EventArgs e)
        {//renglones
           
                tabControl1.SelectedIndex = 4;//tab incial
                arbol_personalizable.marcar(treeView1.Nodes[0]);
                arbol_personalizable.Totalizar();
                Tablas_Datagrid.BindData_Renglon(arbol_personalizable.Arbol_Rengloneado());
                Tablas_Datagrid.pintar_suma(dataGridView4, metroTabPage5);
            
            imprimir(dataGridView4);
        }

        private void treeView1_DoubleClick(object sender, EventArgs e)
        {                                                                              //no recuerdo este metodo
            var localPosition = treeView1.PointToClient(Cursor.Position);
            var hitTestInfo = treeView1.HitTest(localPosition);
            if (hitTestInfo.Location == TreeViewHitTestLocations.StateImage)
                return;
        }

        private void treeView1_AfterCollapse(object sender, TreeViewEventArgs e)
        { // Thread.Sleep(1000);
            Application.DoEvents();
        }

        private void dataGridView5_Scroll(object sender, ScrollEventArgs e)
        {
            int i = tabControl1.SelectedIndex;
            switch (i)
            {
                case 2:
                    dataGridView1.HorizontalScrollingOffset = e.NewValue+5;
                    break;
                case 3:
                    dataGridView3.HorizontalScrollingOffset = e.NewValue+5;
                    break;
                case 4:
                    dataGridView4.HorizontalScrollingOffset = e.NewValue+5;
                    break;
            }
            Application.DoEvents();
        }

        private void treeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
           
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (System.Windows.Forms.OpenFileDialog dialogo = new System.Windows.Forms.OpenFileDialog())
                {
                    if (dialogo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (Stream st = File.Open(dialogo.FileName, FileMode.Open))
                        {
                            var binfor = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                            Stage a = (Stage)binfor.Deserialize(st);
                            cargar_stage(a);
                        }
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {}
        }

        private void guardarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                using (System.Windows.Forms.SaveFileDialog dialogo = new System.Windows.Forms.SaveFileDialog())
                {
                    if (dialogo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        using (Stream st = File.Open(dialogo.FileName, FileMode.Create))
                        {
                            var binfor = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                            binfor.Serialize(st, new Stage(this.forma,this.arbol,this.arbol_personalizable));
                        }
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {}
        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
            int index = int.Parse(dataGridView2.CurrentRow.Cells[0].Value.ToString());
            dataGridView2.CurrentRow.Selected = true;
            Elemento nodo = forma.lista[index];//busqueda de nodo asociado
             if (nodo.isHoja)//si es hoja, es posible editar valores
            {
                Ingreso_valor_PG nuevo = new Ingreso_valor_PG();
                
                nuevo.set_values(nodo,this.Por_defecto.lista[nodo.Id]);///////////////////////////////////////////////////////// poner nodo por default
                nuevo.ShowDialog();
                //solo actualizar DataBind                                 DETECTAR SI HAY CAMBIO
                Tablas_Datagrid.BindData2();
                Tablas_Datagrid.pintar(this.dataGridView2);
                dataGridView2.CurrentCell = dataGridView2.Rows[e.RowIndex].Cells[e.ColumnIndex];
                //modificar valores de los arboles asociados
                arbol.lista[index].Costo= nodo.Costo;
                arbol.lista[index].Cantidad = nodo.Cantidad;
                arbol.Totalizar();
                refresh_label1();//actualizar costo inferior 
                arbol_personalizable.lista[index].Costo = nodo.Costo;
                arbol_personalizable.lista[index].Cantidad = nodo.Cantidad;
            }
            dataGridView2.CurrentRow.Selected = true;
        }
    }
}
