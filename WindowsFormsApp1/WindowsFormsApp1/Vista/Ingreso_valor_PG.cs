﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Vista
{
    public partial class Ingreso_valor_PG : MetroForm
    {
        double costo;
        double cantidad;
        Elemento actual;
        Elemento por_defecto;


        public Ingreso_valor_PG()
        {
            InitializeComponent();
        }

        public void set_values(Elemento current, Elemento p_defecto)
        {
            this.actual = current;
            this.por_defecto = p_defecto;
            
            this.cantidad = actual.Cantidad;
            this.metroTextBox1.Text = "" + this.cantidad;
            this.costo = actual.Costo;
            this.metroTextBox2.Text = "" + this.costo;
        }


        private void calcular()
        {
            if (metroTextBox1.Text.Equals(""))
            {
                cantidad = 0;
            }
            else
            {
                cantidad = double.Parse(metroTextBox1.Text);
            }
            if (metroTextBox2.Text.Equals(""))
            {
                costo = 0;
            }
            else
            {
                costo = double.Parse(metroTextBox2.Text);
            }

            metroLabel4.Text =  ""+ String.Format("{0:#,##0}", cantidad*costo);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {//guardar los valores 
            actual.Costo = this.costo;
            actual.Cantidad = this.cantidad;
            this.Close();
        }

        private void metroTextBox1_ButtonClick(object sender, EventArgs e)
        {
            this.metroTextBox1.Text = "";
        }

        private void metroTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) ||
        char.IsSymbol(e.KeyChar) ||
        char.IsWhiteSpace(e.KeyChar) ||
        char.IsPunctuation(e.KeyChar))
            { e.Handled = true; }
           
        }

        private void metroTextBox2_ButtonClick(object sender, EventArgs e)
        {
            this.metroTextBox2.Text = "";
        }

        private void metroTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) ||
        char.IsSymbol(e.KeyChar) ||
        char.IsWhiteSpace(e.KeyChar) ||
        char.IsPunctuation(e.KeyChar))
            { e.Handled = true;

            }
            
        }

        private void metroTextBox1_TextChanged(object sender, EventArgs e)
        {
           calcular(); 
        }

        private void metroTextBox2_TextChanged(object sender, EventArgs e)
        {
          calcular();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.metroTextBox1.Text = por_defecto.Cantidad+"";
            this.metroTextBox2.Text = por_defecto.Costo + "";    
        }
    }
}
