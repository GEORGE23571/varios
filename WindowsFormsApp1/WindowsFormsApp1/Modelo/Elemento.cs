﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WindowsFormsApp1.Modelo;

namespace WindowsFormsApp1
{
    [Serializable]
    public class Elemento 
    {
        public int  Id { get; set; }
        public string Renglon { get; set; }
        public string Nombre { get; set; }
        public double Cantidad { get; set; }
        public double Costo { get; set; }
        public int Porcentaje=100;//porcentaje a 100% inicialmente
        public double Total { get; set; }
        public bool isHoja { get; set; }
        public int Nivel { get; set; }
        public int opcional = 0;
        public List<int> Unique_selection;// lista de seleccion unica
        public double[] Totales = new double[10]; //totales a 10 anios 
        public double[] Cantidades = new double[10]; //totales a 10 anios
        
        public double[] Porcentajes = new double[10];//porcentaje de los diez anios
        public bool Selected;//indica si se encuentra seleccionado
        public double Cantidad2;
        public double Total2;
        public int[] x= new int[10];//atributos de dependencias
        public int [] y= new int[10];
        public int []z= new int[10];
        public string expresion;//expresion que ajusta totales
        public Elemento Padre;
        public List<Elemento> hijos = new List<Elemento>();// Lista de elementos
        public Arbol a;
        public int anios = 10; //cantidad de anios a trabajar, por default sera 10
        public bool seleccion_default { get; set; }//para la ODAS que requieren seleccion, indica cual es la que debe se marcada en la seleccion  default
        public int Acumulable;//cantidad de anios que debe ser acumulable el gasto

        public void Totalizar()
        {
            if (this.Nombre.Equals("RAIZ")) return;//ignorar si es el nodo raiz

            int residuo;
            this.Cantidad2 = this.Cantidad*((double)Porcentaje)/((double)100); //porcentaje de la meta a trabajar 
           
            residuo = (int)(this.Cantidad2 % this.anios);////                                  funcion ajuste %%%%%%
           
            for (int i = 0; i < residuo; i++)
            {
                if (this.isHoja)
                {
                    if (x[i]>0)
                    {
                        //this.Porcentajes[i] =a.porcentaje(y[i], z[i]);
                    }
                    Cantidades[i] =((this.Cantidad2 * Porcentajes[i]) / 100)+1;

                    if (this.Acumulable != 0 && (i-Acumulable)>-1)
                    {
                        Cantidades[i] += Cantidades[i-Acumulable];
                    }
                    Totales[i] = Cantidades[i] * Costo;
                }
                //subir resultados al padre
                Padre.Totales[i] += Totales[i];
            }

            for (int i = residuo; i < this.anios; i++)
            {
                if (this.isHoja)
                {
                    if (x[i]>0)
                    {
                        //this.Porcentajes[i] = Form1.arbol_personalizable.porcentaje(y[i], z[i]);
                    }

                    Cantidades[i] = ((this.Cantidad2 * Porcentajes[i]) / ((double)100));

                    if (this.Acumulable != 0 && (i - Acumulable) > -1)
                    {
                        Cantidades[i] += Cantidades[i - Acumulable];
                    }
                    Totales[i] = Cantidades[i] * Costo;
                }//subir resultados al padre
                Padre.Totales[i] += Totales[i];
            }

            //borrar calculos previos a nodos complemento
            for (int i = this.anios;i<10;i++)
            {
                Cantidades[i] = 0;
                Totales[i] = 0;
            }

            if (isHoja)
            {
                //Total = (Total2 * this.Porcentaje) / 100; debe ser la suma de los anios trabajados
                Total = 0;
                //MessageBox.Show("Total");
                for (int i =0;i<this.anios;i++)
                {
                    Total += Totales[i];
                }
            }
             Padre.Total += Total;
        }

        public Elemento Clone()
        {
            Elemento n = this.CloneElemento();
            Clone(this,n);
            return n;
        }

        private  void Clone(Elemento original, Elemento nuevo)
        {
            foreach (Elemento e in original.hijos)
            {
                Elemento n = e.CloneElemento();
                n.Padre = nuevo;//padre
                nuevo.hijos.Add(n);
                Clone(e,n);
            }
        }

        private Elemento CloneElemento()
        {
            Elemento e = new Elemento();
            e.Id = this.Id;
            e.Renglon = this.Renglon;
            e.Nombre = this.Nombre;
            e.Cantidad = this.Cantidad;
            e.Costo = this.Costo;
            e.Porcentaje = this.Porcentaje;
            e.Total = this.Total;
            e.isHoja = this.isHoja;
            e.Nivel = this.Nivel;
            e.opcional = this.opcional;
            e.Unique_selection = this.Unique_selection;
            e.expresion = this.expresion;
            e.Total2 = this.Total2;
            e.anios = this.anios;
            e.seleccion_default = this.seleccion_default;
            for (int i = 0; i < 10; i++)
            {
                e.Porcentajes[i] = this.Porcentajes[i];
                e.x[i] = this.x[i];
                e.y[i] = this.y[i];
                e.z[i] = this.z[i];
            }
            e.Acumulable = this.Acumulable;
            return e;
        }

        public void HeredarPorcentaje()//establece y hereda porcentaje
        {
            HeredarPorcentaje(this);
        }

        private void HeredarPorcentaje(Elemento e)
        {
            foreach (Elemento hijo in e.hijos)//aplicar a todos los hijos
            {
                hijo.Porcentaje = e.Porcentaje;
                hijo.set_anios_trabajo(e.anios);
                for (int i =0;i<10;i++)//porcentaje de los 10 años
                {
                    hijo.Porcentajes[i] = e.Porcentajes[i];
                }
                    HeredarPorcentaje(hijo);
            }
        }

      

        public void set_anios_trabajo(int cant_anios)
        {
            
            this.anios = cant_anios;

            double porc = 100 / cant_anios;
            for (int i = 0; i < cant_anios; i++)
            {
                this.Porcentajes[i] = (int)porc;
            }

            //residuo
            int residuo = 100 % cant_anios;
            for (int i = 0; i < residuo; i++)
            {
                this.Porcentajes[i]++;
            }


            for (int i = cant_anios; i < 10; i++)
            {

                this.Porcentajes[i] = 0;
            }
        }
    }
}