﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Collections;
using WindowsFormsApp1.Modelo;

namespace WindowsFormsApp1
{
    class Manager_db
    {

        public static Arbol Arbol;
        public static List<Renglon> Renglones = new List<Renglon>();

        private const string DBName = "DB.db";

        public static SQLiteConnection GetInstance()
        {
            var db = new SQLiteConnection(string.Format("Data Source={0};Version=3;New=True;Compress=True;", DBName));
            db.Open();
            return db;
        }

        public static List<Elemento> getForma()
        {
            List<Elemento> result = new List<Elemento>();

            using (var ctx = Manager_db.GetInstance())
            {
                var query = "SELECT * FROM Forma;";
                double costo = 0, cantidad = 0;
                int id=0,acumulable;
                int opcion;
                int seleccion_default = 0;
                double total=0;
                string renglon;
                int[] x = new int[10];
                int[] y = new int[10];
                int[] z = new int[10];
                List<Elemento> A = new List<Elemento>();
                List<int> selection;

                string cadena = "";
                using (var command = new SQLiteCommand(query, ctx))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cantidad = costo=acumulable=opcion=0;
                            seleccion_default = 0;
                            total = -1;
                            selection = new List<int>();
                            double.TryParse(reader["costo"].ToString(),out costo);
                            double.TryParse(reader["cantidad"].ToString(), out cantidad);
                            int.TryParse(reader["id"].ToString(), out id);
                            int.TryParse(reader["acumulable"].ToString(), out acumulable);
                            int.TryParse(reader["opcional"].ToString(), out opcion);
                            double.TryParse(reader["total"].ToString(), out total);
                            int.TryParse(reader["selection_default"].ToString(), out seleccion_default);

                            cadena = reader["seleccion"].ToString();

                            string [] v = cadena.Split(',');
                            foreach(string n in v)
                            {
                                if (!n.Equals(""))
                                { selection.Add(int.Parse(n));
                                }
                            }

                            renglon = reader["renglon"].ToString().Trim();
                            if (renglon.Length<3)//asuminedo que los renglones tienen 0 al inicio y son de al menos 3 digitos, agregar un cero a lo que tengan menos de 3 cifras
                            {
                                renglon = "0" + renglon;
                            }
                            //arraylist de seleccion unica
                            Elemento e = new Elemento
                            {
                                Id = id,
                                Renglon = renglon,
                                Nombre = reader["nombre"].ToString().TrimStart(),
                                Cantidad = cantidad,
                                Costo = costo,
                                Unique_selection = selection,
                                Acumulable = acumulable,
                                opcional = opcion,
                                expresion = reader["expresion_total"].ToString().TrimStart(),
                                Total2 = total

                            };
                            
                            if (seleccion_default==1)//determina si tiene seleccion default
                            {
                                e.seleccion_default = true;
                            }

                            for (int i = 0; i < 10; i++)
                            {
                                x[i] = -1;
                                y[i] = -1;
                                z[i] = -1;
                                e.x[i] = x[i];
                                e.y[i] = y[i];
                                e.z[i] = z[i];
                            }

                            if (!e.Nombre.Equals("RAIZ"))
                                result.Add(e);
                            //construir el arbol asociado
                            A.Insert(Int32.Parse(reader["id"].ToString().Trim()),e);
                            //Agregarlo como hijo del padre asociado
                            if (Int32.Parse(reader["id"].ToString()) != 0)//es la raiz
                            {
                                A.ElementAt(Int32.Parse(reader["padre"].ToString())).hijos.Add(e);
                            }
                            //agregar referencia al padre
                            e.Padre = A.ElementAt(Int32.Parse(reader["padre"].ToString()));
                        }
                    }
                }
                Manager_db.Arbol = new Arbol(A) ;
            }
            get_renglones();
            return result;
        }
      
        private static void get_renglones()
        {
            using (var ctx = Manager_db.GetInstance())
            {
                var query = "select * from renglon;";
                string renglon;
                using (var command = new SQLiteCommand(query, ctx))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            renglon = reader["id"].ToString().Trim();
                            if (renglon.Length < 3)//asuminedo que los renglones tienen 0 al inicio y son de al menos 3 digitos, agregar un cero a lo que tengan menos de 3 cifras
                            {
                              renglon = "0" + renglon;
                            }

                            Renglones.Add
                                (
                                    new Renglon
                                        {
                                           Nombre= reader["nombre"].ToString(),
                                           Id= renglon
                                        }
                                );
                        }
                    }
                }
            }
        }
        
        public static string name_renglon(string id )
        {
            string name="Renglon no encontrado";
             
            foreach (Renglon r in Renglones)
            {
                if (r.Id.Equals(id))
                {
                    return r.Nombre;
                }
            }
            return name;
        }
    }
}
