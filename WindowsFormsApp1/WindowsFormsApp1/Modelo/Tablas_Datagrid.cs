﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Controlador;

namespace WindowsFormsApp1.Modelo
{
    class Tablas_Datagrid
    {
        private static DataTable custTable = new DataTable("FORMA");//tablas de los primero grid
        private static DataTable custTable2 = new DataTable("FORMA2");
        private static DataTable custTableConfigurable = new DataTable("CONFIGURABLE");//tabla de metas configurable por el usuario
        private static DataTable custTableRenglon = new DataTable("RENGLON");//tabla que sirve para renglonear plan

        private static  Arbol arbol;
        private static Arbol plan;
        private static Arbol arbol_personalizable;
        static int year = 2020;

        private static DataGridView dataGridView1;
        private static DataGridView dataGridView2;
        private static DataGridView dataGridView3;
        private static DataGridView dataGridView4;
        private static DataGridView dataGridView5;

        public static void crear_tablas(DataGridView a, DataGridView b, DataGridView c, DataGridView d, DataGridView e)
        {
            Tablas_Datagrid.dataGridView1 =a;
            Tablas_Datagrid.dataGridView2 =b;
            Tablas_Datagrid.dataGridView3 =c;
            Tablas_Datagrid.dataGridView4 =d;
            Tablas_Datagrid.dataGridView5 = e;
            
            CreateTable2();
            CreateTable1();
            CreateTable_personalizable();
            CreateTable_Renglon();
            
        }

        public static void cargar_stage(Stage stage)
        {
            arbol = stage.arbol_forma;
            arbol_personalizable = stage.arbol_personalizable;
            plan = stage.arbol_planificacion;
            BindData2();
            pintar(dataGridView2);
        }

        private static void CreateTable2()
        {
            // Create a new DataTable.    
            DataColumn dtColumn;
            DataSet dtSet;
            // Create Renglon column  
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(Int32);
            dtColumn.ColumnName = "id2";
            dtColumn.Caption = "ID";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = false;

            // Add column to the DataColumnCollection.  
            custTable2.Columns.Add(dtColumn);

            // Create Renglon column  
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(String);
            dtColumn.ColumnName = "renglon2";
            dtColumn.Caption = "Renglón";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = true;

            // Add column to the DataColumnCollection.  
            custTable2.Columns.Add(dtColumn);

            // Create Nombre column.    
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(String);
            dtColumn.ColumnName = "nombre2";
            dtColumn.Caption = "Nombre";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = true;

            /// Add column to the DataColumnCollection.  
            custTable2.Columns.Add(dtColumn);

            // Create Cantidad column.    
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "cantidad2";
            dtColumn.Caption = "Cantidad";
            dtColumn.ReadOnly = true;
            dtColumn.Unique = false;
            // Add column to the DataColumnCollection.    
            custTable2.Columns.Add(dtColumn);

            // Create Costo column.    
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "costo2";
            dtColumn.Caption = "Costo";
            dtColumn.ReadOnly = true;
            dtColumn.Unique = false;
            // Add column to the DataColumnCollection.    
            custTable2.Columns.Add(dtColumn);

            // Create Costo column.    
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "total2";
            dtColumn.Caption = "Total";
            dtColumn.ReadOnly = true;
            dtColumn.Unique = false;
            // Add column to the DataColumnCollection.    
            custTable2.Columns.Add(dtColumn);
            // Create a new DataSet  
            dtSet = new DataSet();
            // Add custTable to the DataSet.    
            dtSet.Tables.Add(custTable2);
            // Create a BindingSource  
            BindingSource bs = new BindingSource();
            bs.DataSource = dtSet.Tables["FORMA2"];
            // Bind data to DataGridView.DataSource  
            dataGridView2.DataSource = bs;

            foreach (DataGridViewColumn col in dataGridView2.Columns)
            {
                col.HeaderText = custTable2.Columns[col.HeaderText].Caption;
            }
        }

        private static void CreateTable1()
        {
            // Create a new DataTable.    
            DataColumn dtColumn;
            // Create Renglon column  
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(Int32);
            dtColumn.ColumnName = "id";
            dtColumn.Caption = "ID";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = false;

            // Add column to the DataColumnCollection.  
            custTable.Columns.Add(dtColumn);

            // Create Renglon column  
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(String);
            dtColumn.ColumnName = "renglon";
            dtColumn.Caption = "Renglón";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = true;

            // Add column to the DataColumnCollection.  
            custTable.Columns.Add(dtColumn);

            // Create Nombre column.    
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(String);
            dtColumn.ColumnName = "nombre";
            dtColumn.Caption = "Nombre";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = true;
            /// Add column to the DataColumnCollection.  
            custTable.Columns.Add(dtColumn);


            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "porcentaje";
            dtColumn.Caption = "Porcentaje";
            dtColumn.ReadOnly = true;
            dtColumn.Unique = false;
            // Add column to the DataColumnCollection.    
            custTable.Columns.Add(dtColumn);


            // Create Cantidad column.    
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "cantidad";
            dtColumn.Caption = "Cantidad";
            dtColumn.ReadOnly = true;
            dtColumn.Unique = false;
            // Add column to the DataColumnCollection.    
            custTable.Columns.Add(dtColumn);

            // Create Costo column.    
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "costo";
            dtColumn.Caption = "Costo";
            dtColumn.ReadOnly = true;
            dtColumn.Unique = false;
            // Add column to the DataColumnCollection.    
            custTable.Columns.Add(dtColumn);

            // Create Costo column.    
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "total";
            dtColumn.Caption = "Total";
            dtColumn.ReadOnly = true;
            dtColumn.Unique = false;
            // Add column to the DataColumnCollection.    
            custTable.Columns.Add(dtColumn);

            //agregamos 10 columnas para los 10 anios
            for (int i = 0; i < 10; i++)
            {
                // Create Meta column.    
                dtColumn = new DataColumn();
                dtColumn.DataType = typeof(string);
                dtColumn.ColumnName = "porcentaje" + i;
                dtColumn.Caption = "Porcentaje \n" + year;
                dtColumn.ReadOnly = true;
                dtColumn.Unique = false;
                // Add column to the DataColumnCollection.    
                custTable.Columns.Add(dtColumn);

                // Create Meta column.    
                dtColumn = new DataColumn();
                dtColumn.DataType = typeof(string);
                dtColumn.ColumnName = "meta" + i;
                dtColumn.Caption = "Meta \n" + year;
                dtColumn.ReadOnly = true;
                dtColumn.Unique = false;
                // Add column to the DataColumnCollection.    
                custTable.Columns.Add(dtColumn);

                // Create Costo column.    
                dtColumn = new DataColumn();
                dtColumn.DataType = typeof(string);
                dtColumn.ColumnName = "costo" + i;
                dtColumn.Caption = "Costo \n" + year++;
                dtColumn.ReadOnly = true;
                dtColumn.Unique = false;
                // Add column to the DataColumnCollection.    
                custTable.Columns.Add(dtColumn);
            }
            // Create a new DataSet  
            DataSet dtSet = new DataSet();
            // Add custTable to the DataSet.    
            dtSet.Tables.Add(custTable);
            // Create a BindingSource  
            BindingSource bs = new BindingSource();
            bs.DataSource = dtSet.Tables["FORMA"];
            // Bind data to DataGridView.DataSource  
            dataGridView1.DataSource = bs;
            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                col.HeaderText = custTable.Columns[col.HeaderText].Caption;
            }
        }

        private static void CreateTable_personalizable()
        {
            // Create a new DataTable.    
            DataColumn dtColumn;
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(Int32);
            dtColumn.ColumnName = "id";
            dtColumn.Caption = "ID";
            dtColumn.Unique = false;
            custTableConfigurable.Columns.Add(dtColumn);

            year = year - 10;

            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(String);
            dtColumn.ColumnName = "renglon";
            dtColumn.Caption = "Renglón";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = true;
            custTableConfigurable.Columns.Add(dtColumn);
   
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(String);
            dtColumn.ColumnName = "nombre";
            dtColumn.Caption = "Nombre";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = true; 
            custTableConfigurable.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "porcentaje";
            dtColumn.Caption = "Porcentaje";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = true;
            custTableConfigurable.Columns.Add(dtColumn);
 
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "cantidad";
            dtColumn.Caption = "Cantidad";
            dtColumn.ReadOnly = true;
            dtColumn.Unique = false;  
            custTableConfigurable.Columns.Add(dtColumn);
    
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "costo";
            dtColumn.Caption = "Costo";
            dtColumn.ReadOnly = true;
            dtColumn.Unique = false;   
            custTableConfigurable.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "total";
            dtColumn.Caption = "Total";
            dtColumn.ReadOnly = true;
            dtColumn.Unique = false;  
            custTableConfigurable.Columns.Add(dtColumn);

            //agregamos 10 columnas para los 10 anios
            for (int i = 0; i < 10; i++)
            { 
                dtColumn = new DataColumn();
                dtColumn.DataType = typeof(string);
                dtColumn.ColumnName = "porcentaje" + i;
                dtColumn.Caption = "Porcentaje \n" + year;
                dtColumn.ReadOnly = true;
                dtColumn.Unique = false;   
                custTableConfigurable.Columns.Add(dtColumn);

                dtColumn = new DataColumn();
                dtColumn.DataType = typeof(string);
                dtColumn.ColumnName = "meta" + i;
                dtColumn.Caption = "Meta \n" + year;
                dtColumn.ReadOnly = true;
                dtColumn.Unique = false;   
                custTableConfigurable.Columns.Add(dtColumn);
 
                dtColumn = new DataColumn();
                dtColumn.DataType = typeof(string);
                dtColumn.ColumnName = "costo" + i;
                dtColumn.Caption = "Costo \n" + year++;
                dtColumn.ReadOnly = true;
                dtColumn.Unique = false;   
                custTableConfigurable.Columns.Add(dtColumn);
            }
            year = year - 10;

            // Create a new DataSet  
            DataSet dtSet = new DataSet();
            // Add custTable to the DataSet.    
            dtSet.Tables.Add(custTableConfigurable);
            // Create a BindingSource  
            BindingSource bs = new BindingSource();
            bs.DataSource = dtSet.Tables["CONFIGURABLE"];
            // Bind data to DataGridView.DataSource  
            dataGridView3.DataSource = bs;

            foreach (DataGridViewColumn col in dataGridView3.Columns)
            {
                col.HeaderText = custTableConfigurable.Columns[col.HeaderText].Caption;
            }
        }

        private static  void CreateTable_Renglon()
        {
            DataColumn dtColumn;

            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(Int32);
            dtColumn.ColumnName = "id";
            dtColumn.Caption = "ID";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = false;
            custTableRenglon.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(String);
            dtColumn.ColumnName = "renglon";
            dtColumn.Caption = "Renglón";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = true;
            custTableRenglon.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(String);
            dtColumn.ColumnName = "nombre";
            dtColumn.Caption = "Nombre";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = true;
            custTableRenglon.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "porcentaje";
            dtColumn.Caption = "Porcentaje";
            dtColumn.Unique = false;
            dtColumn.ReadOnly = true;
            custTableRenglon.Columns.Add(dtColumn);
            
            dtColumn = new DataColumn();
            dtColumn.DataType = typeof(string);
            dtColumn.ColumnName = "total";
            dtColumn.Caption = "Total";
            dtColumn.ReadOnly = true;
            dtColumn.Unique = false;
            custTableRenglon.Columns.Add(dtColumn);

            for (int i = 0; i < 10; i++)//agregamos 10 columnas para los 10 anios
            {
                dtColumn = new DataColumn();
                dtColumn.DataType = typeof(string);
                dtColumn.ColumnName = "porcentaje" + i;
                dtColumn.Caption = "Porcentaje \n" + year;
                dtColumn.ReadOnly = true;
                dtColumn.Unique = false;
                custTableRenglon.Columns.Add(dtColumn);

                dtColumn = new DataColumn();
                dtColumn.DataType = typeof(string);
                dtColumn.ColumnName = "costo" + i;
                dtColumn.Caption = "Costo \n" + year++;
                dtColumn.ReadOnly = true;
                dtColumn.Unique = false;
                custTableRenglon.Columns.Add(dtColumn);
            }

            DataSet dtSet = new DataSet();
            dtSet.Tables.Add(custTableRenglon);
            BindingSource bs = new BindingSource();
            bs.DataSource = dtSet.Tables["RENGLON"];
            dataGridView4.DataSource = bs;

            foreach (DataGridViewColumn col in dataGridView4.Columns)
            {
                col.HeaderText = custTableRenglon.Columns[col.HeaderText].Caption;
            }
        }

        public static void BindData2()
        {//tabla de totales generales
            custTable2.Rows.Clear();//LIMPIA LA TABLA
            DataRow myDataRow;
            //totalizar 
            foreach (Elemento e in arbol.lista)
            {
                e.Selected = true;
                e.Total = 0;
                for (int i=0;i<10;i++)
                {
                    e.Totales[i] = 0;
                }
            }
            arbol.Totalizar();
            //Iterar sobre todos los marcados y obtener atributos asociados
            foreach (Elemento nodo in arbol.lista)
            {
                if (nodo.Nombre.ToLower().Equals("raiz") )
                {
                    continue;
                }
                //omitir detalle de suledo para mejorar vista de la tabla
                //if (nodo.Renglon.Equals("021") ||
                //    nodo.Renglon.Equals("017") ||
                //    nodo.Renglon.Equals("071") ||
                //    nodo.Renglon.Equals("072")||
                //    nodo.Renglon.Equals("011")
                //    )
                //{
                //    continue;
                //}
                myDataRow = custTable2.NewRow();
                myDataRow["renglon2"] = nodo.Renglon;
                myDataRow["nombre2"] = nodo.Nombre;
                if (nodo.isHoja && nodo.Cantidad > 0)
                    myDataRow["cantidad2"] = String.Format("{0:#,##0}", nodo.Cantidad);
                if (nodo.isHoja && nodo.Costo > 0)
                    myDataRow["costo2"] = String.Format("{0:#,##0}", nodo.Costo);
                myDataRow["id2"] = nodo.Id;
                myDataRow["total2"] = String.Format("{0:#,##0}", nodo.Total);
                custTable2.Rows.Add(myDataRow);
            }
            //ordenar por la columna id
            dataGridView2.Sort(dataGridView2.Columns["id2"], ListSortDirection.Ascending);
            dataGridView2.Columns["id2"].Visible = false;
            dataGridView2.Columns["renglon2"].Visible = false;
        }

        public static void BindData1()
        {
            custTable.Rows.Clear();//LIMPIA LA TABLA
            DataRow myDataRow;
            //Iterar sobre todos los marcados y obtener atributos asociados
            foreach (Elemento nodo in plan.nodes_cheked)
            {
                myDataRow = custTable.NewRow();
                myDataRow["renglon"] = nodo.Renglon;
                myDataRow["nombre"] = nodo.Nombre;
                if (nodo.isHoja && nodo.Cantidad > 0)
                {
                    myDataRow["cantidad"] = String.Format("{0:#,##0}", nodo.Cantidad);
                }
                if (nodo.isHoja && nodo.Costo > 0)
                {
                    myDataRow["costo"] = String.Format("{0:#,##0}", nodo.Costo);
                }
                myDataRow["id"] = nodo.Id;
                myDataRow["total"] = String.Format("{0:#,##0}", nodo.Total);
                myDataRow["porcentaje"] = String.Format("100%", nodo.Total);

                //metas en los 10 anios
                for (int i = 0; i < 10; i++)
                {
                    if (nodo.Cantidades[i] != 0)
                        myDataRow["meta" + i] = String.Format("{0:#,##0}", nodo.Cantidades[i]);
                    if (nodo.Totales[i] != 0)
                        myDataRow["costo" + i] = String.Format("{0:#,##0}", nodo.Totales[i]);
                    if (nodo.Nivel == 1)
                        myDataRow["porcentaje" + i] = String.Format("{0:#,##0}%", nodo.Porcentajes[i]);
                }
                custTable.Rows.Add(myDataRow);
            }
            //fila de totales
            myDataRow = custTable.NewRow();
            myDataRow["total"] = String.Format("{0:#,##0}", plan.Raiz.Total);
            myDataRow["nombre"] = "TOTAL";
            myDataRow["id"] = "1000";

            for (int i = 0; i < 10; i++)
            {
                if (plan.Raiz.Totales[i] != 0)
                    myDataRow["costo" + i] = String.Format("{0:#,##0}", plan.Raiz.Totales[i]);
            }

            custTable.Rows.Add(myDataRow);
            //ordenar por la columna id
            dataGridView1.Sort(dataGridView1.Columns["id"], ListSortDirection.Ascending);
            dataGridView1.Columns["id"].Visible = false;
            dataGridView1.Columns["renglon"].Visible = false;
            pintar(dataGridView1);
        }

        public static  void BindData_personalizable()
        {
            custTableConfigurable.Rows.Clear();//LIMPIA LA TABLA
            DataRow myDataRow;
            //Iterar sobre todos los marcados y obtener atributos asociados
            foreach (Elemento nodo in arbol_personalizable.nodes_cheked)
            {
                myDataRow = custTableConfigurable.NewRow();
                myDataRow["renglon"] = nodo.Renglon;
                myDataRow["nombre"] = nodo.Nombre;
                if (nodo.isHoja && nodo.Cantidad > 0)
                { myDataRow["cantidad"] = String.Format("{0:#,##0}", nodo.Cantidad2); }
                if (nodo.isHoja && nodo.Costo > 0)
                { myDataRow["costo"] = String.Format("{0:#,##0}", nodo.Costo); }
                myDataRow["id"] = nodo.Id;
                myDataRow["total"] = String.Format("{0:#,##0}", nodo.Total);
                myDataRow["porcentaje"] = String.Format("{0}%", nodo.Porcentaje);

                //metas en los 10 anios
                for (int i = 0; i < 10; i++)
                {
                   if (nodo.Cantidades[i] != 0)
                        myDataRow["meta" + i] = string.Format("{0:#,##0}", nodo.Cantidades[i]);
                   if (nodo.Totales[i] != 0)
                        myDataRow["costo" + i] = String.Format("{0:#,##0}", nodo.Totales[i]);
                   if (nodo.Nivel == 1)
                    myDataRow["porcentaje" + i] = String.Format("{0:#,##0}%", nodo.Porcentajes[i]);
                }
                custTableConfigurable.Rows.Add(myDataRow);
            }
            //fila de totales
            myDataRow = custTableConfigurable.NewRow();
           myDataRow["total"] = String.Format("{0:#,##0}", arbol_personalizable.Raiz.Total);
            myDataRow["nombre"] = "TOTAL";
            myDataRow["id"] = "1000";

            for (int i = 0; i < 10; i++)
            { 
               myDataRow["costo" + i] = String.Format("{0:#,##0}", arbol_personalizable.Raiz.Totales[i]);
            }
            custTableConfigurable.Rows.Add(myDataRow);
            //ordenar por la columna id
            dataGridView3.Sort(dataGridView3.Columns["id"], ListSortDirection.Ascending);
            dataGridView3.Columns["id"].Visible = false;
            dataGridView3.Columns["renglon"].Visible = false;
            pintar(dataGridView3);
        }

        public static  void BindData_Renglon(Arbol  arbol_rengloneado)
        {
            custTableRenglon.Rows.Clear();//LIMPIA LA TABLA
            DataRow myDataRow;
            foreach (Elemento nodo in arbol_rengloneado.lista) //Iterar sobre todos los marcados y obtener atributos asociados
            {
                myDataRow = custTableRenglon.NewRow();
                myDataRow["renglon"] = nodo.Renglon;
                if (nodo.isHoja)
                {
                    myDataRow["nombre"] = Manager_db.name_renglon(nodo.Renglon);
                }
                else
                {
                    myDataRow["nombre"] = nodo.Nombre;
                    myDataRow["porcentaje"] = String.Format("{0}%", nodo.Porcentaje);
                }

                myDataRow["id"] = nodo.Id;
                myDataRow["total"] = String.Format("{0:#,##0}", nodo.Total);

                for (int i = 0; i < 10; i++)//metas en los 10 anios
                {
                    if (nodo.Totales[i] != 0)
                        myDataRow["costo" + i] = String.Format("{0:#,##0}", nodo.Totales[i]);
                     if (nodo.Nivel == 1)
                    myDataRow["porcentaje" + i] = String.Format("{0}%", nodo.Porcentajes[i]);
                }

                custTableRenglon.Rows.Add(myDataRow);
            }

            myDataRow = custTableRenglon.NewRow();
            myDataRow["total"] = String.Format("{0:#,##0}",arbol_personalizable.Raiz.Total);
            myDataRow["nombre"] = "TOTAL";
            myDataRow["id"] = "1000";
            for (int i = 0; i < 10; i++)
            {
               // if (arbol.Raiz.Totales[i] != 0)
                    myDataRow["costo" + i] = String.Format("{0:#,##0}", arbol_personalizable.Raiz.Totales[i]);
            }

            custTableRenglon.Rows.Add(myDataRow);

            for (int i = 0; i < 10; i++)
            {
                //dataGridView4.Columns["costo" + i].Visible = false;
                dataGridView4.Columns["porcentaje" + i].Visible = false;
            }
            dataGridView4.Sort(dataGridView4.Columns["id"], ListSortDirection.Ascending);
            dataGridView4.Columns["id"].Visible = false;
            pintar(dataGridView4);
        }

        public static void pintar(DataGridView dataGridView)
        {//recorrer datos en el datagrid
            Elemento p;
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            dataGridView.ColumnHeadersDefaultCellStyle.BackColor = Color.DarkOrange;
            dataGridView.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 8F, FontStyle.Bold);
            dataGridView.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView.EnableHeadersVisualStyles = false;
            dataGridView.RowHeadersVisible = false;

            for (int i = 0; i < dataGridView.Rows.Count -1; i++)
            {
                p = arbol.ElementAt(Int32.Parse(dataGridView.Rows[i].Cells[0].Value.ToString()));
                dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.White;
                if (!p.isHoja)
                {
                    dataGridView.Rows[i].DefaultCellStyle.ForeColor = Color.White;//color de letra a ramas
                    
                    switch (p.Nivel)
                    {
                        case 1:
                            dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(38, 50, 56);
                            break;
                        case 2:
                            dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(84, 110, 122);
                            break;
                        case 3:
                            dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(144, 164, 174);
                            break;
                        case 4:
                            dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(190, 201, 208);
                            break;
                        case 5:
                            dataGridView.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(236, 239, 241);
                            dataGridView.Rows[i].DefaultCellStyle.ForeColor = Color.Gray;//color de letra a ramas
                            break;
                    }

                }
                dataGridView.Rows[i].DefaultCellStyle.Font = new Font("Arial", 8F, FontStyle.Bold);//tamano letra y negrilla
            }
            dataGridView.Rows[dataGridView.Rows.Count - 1].DefaultCellStyle.Font = new Font("Arial", 8F, FontStyle.Bold);//tamano letra y negrilla

            if (dataGridView.Rows[dataGridView.RowCount - 1].Cells[2].Value.ToString().Equals("TOTAL"))
            {
                dataGridView.Rows[dataGridView.RowCount - 1].DefaultCellStyle.Font = new Font("Arial", 10F, FontStyle.Bold);
                dataGridView.Rows[dataGridView.RowCount - 1].DefaultCellStyle.ForeColor = Color.White;
                dataGridView.Rows[dataGridView.RowCount - 1].DefaultCellStyle.BackColor = Color.DarkBlue;
                dataGridView.Columns["nombre"].Frozen = true;
            }
            //dataGridView.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridView.AutoResizeRows();
            dataGridView.AutoResizeColumns();
            dataGridView.Columns[2].Width = 500;

            for (int i = 0; i < 3; i++)
            {
                dataGridView.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            //jsutificar a la derecha cantidades
            for (int i = 3; i < dataGridView.Columns.Count; i++)
            {
                dataGridView.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
                dataGridView.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            dataGridView.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter;
        }

        public static void pintar_suma(DataGridView dataGridView, TabPage tab)
        {
            if (dataGridView.Rows[dataGridView.RowCount - 1].Cells[2].Value.ToString().Equals("TOTAL"))
            {
                dataGridView5.Visible = true;
                dataGridView5.Columns.Clear();//eliminar columnas agregadas
                foreach (DataGridViewColumn col in dataGridView.Columns)
                    {
                        DataGridViewTextBoxColumn Column2 = new DataGridViewTextBoxColumn();
                        Column2.HeaderText = col.HeaderText;
                        Column2.Name = col.HeaderText;
                        dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {Column2});
                        Column2.Visible = col.Visible;
                    }
                    dataGridView5.Rows.Add(1);//agregar nueva fila
                for (int i = 0; i < dataGridView.Columns.Count; i++)
                {
                    dataGridView5.Rows[0].Cells[i].Value = dataGridView.Rows[dataGridView.RowCount - 1].Cells[i].Value;
                    dataGridView5.Rows[0].Cells[i].Style = dataGridView.Rows[dataGridView.RowCount - 1].Cells[i].Style;
                    dataGridView5.Columns[i].Width = dataGridView.Columns[i].Width;
                }

                dataGridView5.Rows[0].DefaultCellStyle.Font = new Font("Arial", 10F, FontStyle.Bold);
                dataGridView5.Rows[0].DefaultCellStyle.ForeColor = Color.White;
                dataGridView5.Rows[0].DefaultCellStyle.BackColor = Color.DarkBlue;
                dataGridView5.RowHeadersVisible = false;
                int pixeles =8+ dataGridView.Height - ((dataGridView.Height - dataGridView.ColumnHeadersHeight) / dataGridView.Rows[0].Height) * dataGridView.Rows[0].Height;
                dataGridView5.Height = pixeles;
                dataGridView5.Width = dataGridView.Width;
                tab.Controls.Add(dataGridView5);
                dataGridView5.Dock = System.Windows.Forms.DockStyle.Bottom;
                dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
                dataGridView5.Columns["nombre"].Frozen = true;
                dataGridView.HorizontalScrollingOffset = dataGridView5.HorizontalScrollingOffset=0;
                //calcular si se pasa del alto del datagridview huesped
                pixeles = dataGridView.ColumnHeadersHeight + dataGridView.Rows[0].Height * dataGridView.Rows.Count;
                if (pixeles > dataGridView.Height)
                {// poner la ultima fila visible
                    dataGridView5.Rows[0].Visible = true;
                    dataGridView.Rows[dataGridView.RowCount - 1].Visible = false;
                }
                else
                {
                    dataGridView5.Rows[0].Visible = false;
                    dataGridView.Rows[dataGridView.RowCount - 1].Visible = true;
                }
            }
        }
    }
}
