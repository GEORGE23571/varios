﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Modelo
{
    [Serializable]
    public class Arbol
    {
        public Elemento Raiz;//raiz del arbol
        public List<Elemento> lista;        //lista de nodos en orden 
        public List<Elemento> nodes_cheked = new List<Elemento>();//lista de nodos seleccionados

        public Arbol Clone()
        {
            Arbol tree = new Arbol();
            List<Elemento> l = new List<Elemento>();

            tree.Raiz = this.Raiz.Clone();
            tree.lista = l;
            for (int i = 0; i < lista.Count; i++)
            {
                l.Add(new Elemento());
            }
            contruir_lista(l, tree.Raiz);
            //meter el primer nodo en la primera posicion
            l[0] = tree.Raiz;

            return tree;
        }

        private void contruir_lista(List<Elemento> l, Elemento Nodo)
        {
            foreach (Elemento e in Nodo.hijos)
            {
                l[e.Id]=e;
                contruir_lista(l,e);
            }
        }

        public Arbol()
        { }

        public Arbol( List<Elemento> l )
        {
            this.Raiz = l.ElementAt(0);
            this.lista = l;
            Establecer_nivel(this.Raiz, 0);
        }

        public Elemento ElementAt(int index)
        {
            return this.lista.ElementAt(index);  
        }

        private  void Establecer_nivel(Elemento raiz, int i)
        {//metodo que realiza la numeracion por niveles en el arbol
            raiz.Nivel = i;//establece el nivel
            foreach (Elemento e in raiz.hijos)
            {
                Establecer_nivel(e, i + 1);
            }

            if (raiz.hijos.Count == 0)
            {
                raiz.isHoja = true;
            }
            else
            {
                raiz.Cantidad = 0;
                raiz.Costo = 0;
            }

            for (int j = 1; j < i; j++)//acomodar texto para visualizar de mejor forma los niveles, tabulacion
            {
                raiz.Nombre = "     " + raiz.Nombre;
            }
        }

        public void marcar(TreeNode nodo_treeview)
        {
            nodes_cheked.Clear(); //limpia la lista de nodos marcados
            marcar(nodo_treeview, this.Raiz); //marca nodos que fueron seleccionados
        }

        private void marcar(TreeNode nodo_treeview, Elemento raiz)
        { //BORRAR VALORES ACTUALES
            raiz.Total = 0;
            for (int i = 0; i < 10; i++)
            {
                raiz.Totales[i] = 0;
            }

            for (int i = 0; i < nodo_treeview.Nodes.Count; i++)
            {
                if (nodo_treeview.Nodes[i].Checked)
                {
                    raiz.hijos[i].Selected = true;
                    marcar(nodo_treeview.Nodes[i], raiz.hijos[i]);//se trabajara unicamente con nodo seleccionados
                    nodes_cheked.Add(raiz.hijos[i]);
                }
                else
                {
                    raiz.hijos[i].Selected = false;
                }
            }
        }

        public void Totalizar()
        {
             totalizar(this.Raiz);
        }

        private void totalizar(Elemento r)
        {//poner en cero antes de totalizar
            r.Total = 0;
            for (int i =0;i<10;i++)
            {
                r.Totales[i] = 0;
            }
            foreach (Elemento nodo in r.hijos)
            {
                if (nodo.Selected)
                {
                     totalizar(nodo);
                     nodo.Totalizar();
                }
            }
        }

        public Arbol Arbol_Rengloneado()
        {
            Arbol a = new Arbol();
            a.Raiz = new Elemento();
            a.lista = new List<Elemento>();
            Arbol_Rengloneado(this.Raiz,a.Raiz,a);
            return a;
        }

        private void Arbol_Rengloneado(Elemento raiz_vieja, Elemento raiz_nueva, Arbol a)
        {
            foreach (Elemento e in raiz_vieja.hijos)
            {
                if (!e.Selected) continue;//considerar solo nodos seleccionados

                if (e.isHoja)//si es hoja insertar en el padre
                {
                    add_renglon(e,raiz_nueva,a);
                }
                else
                {   //es un nodo y no hay eleccion en cuanto a la agregacion.
                    Elemento m = new Elemento();
                    m.Nombre = e.Nombre;
                    m.Padre = raiz_nueva;
                    m.Porcentaje = e.Porcentaje;
                    m.Total = e.Total;
                    m.Id = e.Id;
                    a.lista.Add(m);
                    //los totales no cambian y unicamente hay que copiarlos
                    m.Totales = e.Totales;
                    m.Porcentajes = e.Porcentajes;
                    Arbol_Rengloneado(e,m,a);
                }
            }
        }

        private void add_renglon(Elemento n, Elemento padre,Arbol a)
        {
            foreach (Elemento p in padre.hijos)
            {
                if (p.Renglon.Equals(n.Renglon))
                {//ya existe el renglon, totalizar
                    //p.Cantidad += n.Cantidad;
                    //p.Costo += n.Costo;
                    p.Total += n.Total;
                    return ;
                }
            }
            Elemento e = new Elemento();
            e.Cantidad = n.Cantidad;
            e.Costo = n.Costo;
            e.Total = n.Total;
            e.Id = n.Id;
            //agregar al padre
            padre.hijos.Add(e);
            e.Padre = padre;
            e.Renglon = n.Renglon;
            a.lista.Add(n);
        }

        public int porcentaje(int i, int j)
        {
            return (int)this.lista[i].Porcentajes[j];
        }

        public TreeNode TreeView()
        {
            TreeNode root = new TreeNode("RAIZ");
            TreeView(root,this.Raiz);
            return root;
        }

        private void TreeView(TreeNode nodo_treeview, Elemento Nodo_Elemento)//construye el arbol del componente TreeView
        {
            TreeNode pivote;
            nodo_treeview.Checked = Nodo_Elemento.Selected;
            if (nodo_treeview.Checked)
                nodo_treeview.Expand();
            foreach (Elemento nodo in Nodo_Elemento.hijos)
            {
                pivote = new TreeNode(nodo.Nombre);
                TreeView(pivote, nodo);
                nodo_treeview.Nodes.Add(pivote);
            }
        }

        public void Check_node(TreeNode nodo_treeview)
        {
            Elemento e = Find_node_elemento(nodo_treeview);
            MessageBox.Show(e.Nombre);
        }

        public Elemento Find_node_elemento(TreeNode nodo_treeview)
        {

            if (nodo_treeview.Equals(nodo_treeview.TreeView.Nodes[0]))//si es nodo raiz , retornar raiz del arbol asociado
            {
                return this.Raiz;
            }  

          return   Find_node_elemento(nodo_treeview,nodo_treeview.TreeView.Nodes[0],this.Raiz);
        }

        private Elemento Find_node_elemento(TreeNode nodo_treeview,TreeNode raiz_tv,Elemento raiz)
        {
            for(int i = 0; i < raiz_tv.Nodes.Count; i++)
            {
                if (raiz_tv.Nodes[i].Equals(nodo_treeview))
                {
                    return raiz.hijos[i];
                }
                else
                {
                    Elemento e = Find_node_elemento(nodo_treeview, raiz_tv.Nodes[i], raiz.hijos[i]);
                    if (e!=null)
                    {
                        return e;
                    }
                }
            }
            return null;
        }

        public TreeNode Find_node_tv(int i , TreeNode nodo_treeview)
        {
            return Find_node_tv(i,nodo_treeview.TreeView.Nodes[0],this.Raiz);//hacer la busqueda siempre desde la raiz
        }

        private TreeNode Find_node_tv(int j, TreeNode nodo_treeview,Elemento raiz)
        {
            for (int i = 0;i<raiz.hijos.Count;i++)
            {
                if (raiz.hijos[i].Id==j)
                {
                    return nodo_treeview.Nodes[i];
                }
                else
                {
                    TreeNode e = Find_node_tv(j,nodo_treeview.Nodes[i],raiz.hijos[i]);
                    if (e!=null)
                    {
                        return e;
                    }
                }
            }
            return null;
        }
    }
}
