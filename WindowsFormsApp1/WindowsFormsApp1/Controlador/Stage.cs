﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Modelo;

namespace WindowsFormsApp1.Controlador
{
    [Serializable]
    class Stage
    {
        public Arbol arbol_forma;
        public Arbol arbol_planificacion;
        public Arbol arbol_personalizable;

        public Stage(Arbol forma, Arbol planificacion, Arbol personalizable)
        {
            this.arbol_forma = forma;
            this.arbol_planificacion = planificacion;
            this.arbol_personalizable = personalizable;
            // poner a 100% en un anio el costo en el arbol de forma
            this.arbol_forma.Raiz.Porcentaje = 100;//fijar probabilidades equitativas, estado inicial 
            this.arbol_forma.Raiz.Porcentajes[0] = 100;
            for (int i = 1; i < 10; i++)
            {
                this.arbol_forma.Raiz.Porcentajes[i] = 0;
            }
            this.arbol_forma.Raiz.HeredarPorcentaje();

        }
        public Stage()
        {
            Manager_db.getForma();//llenar el arbol
            Arbol arbol = Manager_db.Arbol;
            this.arbol_forma = arbol.Clone();
            
            this.arbol_planificacion = arbol.Clone();
           
            //                                                  estado inicial del arbol forma
            this.arbol_forma.Raiz.Porcentaje = 100;
            this.arbol_forma.Raiz.Porcentajes[0] = 100;
            for (int i = 1; i < 10; i++)
            {
                this.arbol_forma.Raiz.Porcentajes[i] = 00;
            }
            this.arbol_forma.Raiz.set_anios_trabajo(1);
            foreach (Elemento n in this.arbol_forma.lista)
            {
                n.Selected = false;
            }
            this.arbol_forma.Raiz.HeredarPorcentaje();
            //                                                   estado inicial del arbol personalizable 
            this.arbol_planificacion.Raiz.Porcentaje = 100;
            for (int i = 0; i < 10; i++)
            {
                this.arbol_planificacion.Raiz.Porcentajes[i] = 10;
            }
            foreach (Elemento n in this.arbol_planificacion.lista)
            {
                n.Selected = false;
            }
            this.arbol_planificacion.Raiz.HeredarPorcentaje();
            //                                                       arbol personalizable hereda  porcentajes iniciales 
            this.arbol_personalizable = this.arbol_planificacion.Clone();
        }
    }
}
