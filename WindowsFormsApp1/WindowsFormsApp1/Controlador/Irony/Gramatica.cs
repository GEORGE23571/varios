﻿using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Controlador.Irony
{
    class Gramatica:Grammar
    {
        public Gramatica() : base(caseSensitive:false)
        {
            //expresione regulares
            RegexBasedTerminal numero = new RegexBasedTerminal("numero","[0-9]+");
            RegexBasedTerminal variable = new RegexBasedTerminal("variable", "x|y");
            //terminales
            var mas = ToTerm("+");
            var menos = ToTerm("-");
            var por = ToTerm("*");
            var div = ToTerm("/");
            //NO TERMINALES
            NonTerminal S = new NonTerminal("S"),
                E = new NonTerminal("E"),
                F = new NonTerminal("F"),
                T = new NonTerminal("T");
            //GRAMATICA
            S.Rule = E;
            E.Rule= E+mas+ T
                | E + menos + T
                | T;
            T.Rule = T + por + F
                | T + div + F
                | F;
            F.Rule = numero
                |variable;
            //raiz
            this.Root = S;
        }
    }
}
