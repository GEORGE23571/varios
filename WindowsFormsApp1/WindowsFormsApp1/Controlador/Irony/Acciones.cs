﻿using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WindowsFormsApp1.Controlador.Irony
{
    class Acciones
    {
        static double x;
        static double y;
        public static double operar(string expresion, double x , double y)
        {
            Acciones.x = x;
            Acciones.y = y;
            Sintactico AS= new Sintactico() ;
            ParseTreeNode raiz = AS.analizar(expresion);
            return realizarAccion(raiz);
        }
        private static double realizarAccion( ParseTreeNode raiz)
        {
            return accion(raiz);
        }

        private static double accion(ParseTreeNode nodo)
        {
            double result = 0;
            switch (nodo.Term.Name.ToString())
            {
                case "S":
                    if (nodo.ChildNodes.Count==1)
                    {
                        result = accion(nodo.ChildNodes[0]);
                    }
                    break;
                case "E":
                    if (nodo.ChildNodes.Count == 1)
                    {
                        result = accion(nodo.ChildNodes[0]);
                    }
                    else if (nodo.ChildNodes.Count == 3)
                    {
                        double a = Convert.ToDouble(accion(nodo.ChildNodes[0]).ToString());
                        double b = Convert.ToDouble(accion(nodo.ChildNodes[2]).ToString());
                        if (nodo.ChildNodes[1].Token.Value.ToString()=="+")
                        {
                            result = a + b;
                        }
                        else
                        {
                            result = a - b;
                        }
                    }
                    break;
                case "T":
                    if (nodo.ChildNodes.Count == 1)
                    {
                        result = accion(nodo.ChildNodes[0]);
                    }
                    else if (nodo.ChildNodes.Count == 3)
                    {
                        double a = Convert.ToDouble(accion(nodo.ChildNodes[0]).ToString());
                        double b = Convert.ToDouble(accion(nodo.ChildNodes[2]).ToString());
                        if (nodo.ChildNodes[1].Token.Value.ToString() == "*")
                        {
                            result = a * b;
                        }
                        else
                        {
                            result = a/b;
                        }
                    }
                    break;
                case "F":
                    if (nodo.ChildNodes.Count==1)
                    {
                        result = accion(nodo.ChildNodes[0]);
                    }
                    break;
                case "numero":
                    {
                        result = Convert.ToDouble(nodo.Token.Value.ToString());
                    }
                    break;
                case "variable":
                    {
                        if (nodo.Token.Value.ToString().Equals("x"))
                        {
                            result = Acciones.x;
                        }
                        else
                        {
                            result = Acciones.y;
                        }
                    }
                    break;
            }
            return result;
        }

    }
}
