
package com.mycompany.simulador_demanda_docente;

import Utilidades.Lectura;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import modelo.CentroEducativo;
import modelo.Departamento;
import modelo.Docente;
import modelo.Plaza;

public class Simulador 
{
    private static ArrayList<Departamento> Departamentos;
    private static int holgura ;
    private static int suma_puestos=0;
    private static int suma_total_puestos=0;
    
    
    public static void suma_puestos()
    {
        Simulador.suma_puestos++;
    }
    
    public static void Init( ArrayList<Departamento> Departamentos, int holgura)
    {
        Simulador.Departamentos= Departamentos;
        Afinidad();
       
        Simulador.holgura=holgura;
        
        run();
        
    }
    
    
    
    private static void run ()
    {
    int n=-1;
    int i =1;

        while ((n=setEntrevistas())>0)
        {
            contratacion();
            
            System.out.println("                                        iteracion "+(i)+"  Cantidad de entrevistas "+n+"  Cantidad de plazas ocupadas "+Simulador.suma_puestos);
            Simulador.suma_total_puestos+=Simulador.suma_puestos;
            Simulador.suma_puestos=0;
            i++;
        }
       iteraciones(i);
    }
    
    
    
    public static void iteraciones(int it )
    {
        ArrayList<ArrayList<Integer>> iteraciones = new  ArrayList<ArrayList<Integer>> ();
        
        
        int promedio_iteraciones= 0;
        int cantidad_sin_interes=0;
        for (int i =0;i<it;i++)
        {
            iteraciones.add(new ArrayList<Integer>());
        }

        int k = Lectura.Centros.size();
        for(int i =0; i<k;i++)//RECORRER SOBRE LOS CENTROS
        {
            CentroEducativo ce = Lectura.Centros.get(i);
            for (int j = 0;j<ce.getEntrevistas().size();j++)
            {
                iteraciones.get(j).add(ce.getEntrevistas().get(j));
            }
            
            ArrayList<Plaza> plazas = ce.getPlazas();
            
            for(int j=0;j<plazas.size();j++)
            {
                if(plazas.get(j).getInteresados().isEmpty())
                {
                   cantidad_sin_interes++;
                }
            }
            
            for (int j = 0;j<ce.getEntrevistas().size();j++)
            {
                
            }
            promedio_iteraciones+= ce.getEntrevistas().size();
        }
        
        promedio_iteraciones= (promedio_iteraciones/k);
        
    
        
      //ORDENAR DE MAYOR A MENOR E IMPRIMIR DATOS
      for (int i =0;i<it-1;i++)
        {
            Comparator<Integer> comparador = Collections.reverseOrder();
            Collections.sort( iteraciones.get(i), comparador);
            System.out.println("iteracion "+(i+1)+": maxima cantidad de entrevista por centro  "+iteraciones.get(i).get(0));
        }

        System.out.println("Total de plazas ocupadas "+ Simulador.suma_total_puestos);
        System.out.println("Total de plazas sin interesados "+cantidad_sin_interes);
        //System.out.println("Promedio de participaciones por centro "+promedio_iteraciones);
        
        
        
        
        
        
        
        
    }
    
    
    private static int setEntrevistas()//el periodo de entrevista esta determinado por el centro que mas plazas necesita ocupar
    {
        int centros_por_llenar=0;
        int k = Lectura.Centros.size();
        for(int i =0; i<k;i++)//RECORRER SOBRE LOS CENTROS
        {
           CentroEducativo ce = Lectura.Centros.get(i);
           if (ce.getPlazasLibres()>0)
           {
            System.out.println("Nucleo numero"+i);
           }
          
           int entrevistas=0;
          
           for (int j = 0 ;j<ce.getPlazas().size();j++)//RECORRER SOBRE LAS PLAZAS 
           {
               //!!!!!!!!!!!!!!!!!!!!recorrer solo plazas disponibles
               if(ce.getPlaza(j).isOcupada())
               {
                    continue;
               }
               
               
               Plaza plaza = ce.getPlaza(j);
               
               plaza.sortInteresados();//ORDENAR DE ACUERDO A LA CALIFICACION A LOS INTERESADOS EN LA PLAZA
              
               
                //SELECCIONAR A LOS "N " + "X" DOCENTES PARA OCUPAR X PLAZAS
                //"X" NUMERO DE PLAZAS DISPONIBLES
                //"N" NUMERO DE HOLGURA PARA REALIZAR LA SELECCION DE DOCENTES
                int numero_docentes= plaza.getNombramiento()+holgura-1;
                //a los numero_docentes de la lista de interesados,!! de exitir los suficientes reservarlos de ser posibles
                int x=0;
                if(plaza.getNoInteresados()==0)//trabajr plaza con interesados validos
                {
                    continue;
                }
                System.out.println("        plaza "+plaza.getArea()+"  "+plaza.getTipo()+"  "+ plaza.getNombramiento());
                
                ArrayList<String> texto = new ArrayList<>();
                
                for(int l = 0; l<plaza.getInteresados().size();l++)//RECORRER SOBRE LOS INTERESADOS
                {
                    
                    Docente doc = plaza.getInteresados().get(l);
                    if(doc.getEstado()==0&&(x<numero_docentes))//si esta desocupado, seleccionarlo para cita
                    {
                        doc.setEstado(1);//indicar que no es elegible para otra entrevista
                        plaza.addConvocado(doc);//agrega al docnete en la lista de convocados
                        x++;
                        centros_por_llenar++;
                        
                        texto.add("                Docente:  "+plaza.getInteresados().get(l).getId_docente()+
                                "       Calificacion   "+plaza.getInteresados().get(l).getCalificacion());
                    }
                }
              int aleatorio = (int) (Math.random() * texto.size());
              String cadena="";
              for (int l=0;l<texto.size();l++)
              {
                  cadena += texto.get(l);
                  if(l== aleatorio)
                  {
                      cadena = cadena +"  Contratado";
                  }
                  cadena+="\n";
              }
               System.out.println(cadena);
                entrevistas+= x;//acumula el numero de entrevista
           }
           ce.setEntrevistas(entrevistas);//ingresa el numero de entrevista en la iteracion
        }
        return centros_por_llenar;
    }
    
    
    
    public static void contratacion()//selecciona a uno de los interesado para la plaza
    {
        int k = Lectura.Centros.size();

        for(int i =0; i<k;i++)//RECORRER SOBRE LOS CENTROS
        {
           CentroEducativo ce = Lectura.Centros.get(i);
           for (int j = 0 ;j<ce.getPlazas().size();j++)//RECORRER SOBRE LAS PLAZAS 
           {
               //!!!!!!!!!!!!!!!!!!!!recorrer solo plazas disponibles
               if(ce.getPlaza(j).isOcupada())
               {
                    continue;
               }
                Plaza plaza = ce.getPlaza(j);
                
                for(int m = 0; m<plaza.getNombramiento();m++)
                {
                    if(plaza.getConvocados().size()>0)
                    {
                       plaza.getConvocado(0).setEstado(2);//indicar que es contratado el docnete
                       plaza.getConvocados().remove(0);//quitar al primero en la lista
                       plaza.setNombramiento(plaza.getNombramiento()-1);//disminuir en 1 el nombramiento
                    }   
                }
                if(plaza.getNombramiento()==0)//si ya no hay nombramientos pendientes
                {
                    plaza.setOcupada(true);
                }
                
                plaza.removeAllConvocados();//vacia la lista de convocados y los libera
               // plaza.setOcupada(true);//indicar que la plaza ha sido ocupada
           }
        }
    
    }
    
    private static  void Afinidad ()//este metodo agrega a los docentes a lo centros segun municipio, plaza , centro
    {
        int k = Simulador.Departamentos.size();//recorrer todos los departamentos
        for (int i = 0;i<k;i++)
        {
            Departamento p =Simulador.Departamentos.get(i); 
            int kk= p.municipios.size();
            
            for (int j=0;j<kk;j++)//recorrer todos los municipios del departamento
            {
                Afinidad(p.municipios.get(j).docentesMAT,p.municipios.get(j).getCentros());
                  Afinidad(p.municipios.get(j).docentesCYL,p.municipios.get(j).getCentros());
                    Afinidad(p.municipios.get(j).docentesCNN,p.municipios.get(j).getCentros());
                      Afinidad(p.municipios.get(j).docentesCS,p.municipios.get(j).getCentros());
            }
        }
    }
    
    private static  void Afinidad(ArrayList<Docente> Docentes, ArrayList<CentroEducativo> Centros)
    {
        //todos son ponderables por el docente 
        int k = Docentes.size();
        int cant_centros= Centros.size();
        
        for(int i = 0; i<k;i++)//recorre todos los docentes 
        {
              int inicio = (int) (Math.random()*cant_centros) ;
              
              for(int x= inicio; x<cant_centros;x++)
              {
                  //verifica si hay un plaza adecuada para el docente (area y futuramente la cantidad de periodos)
                  CentroEducativo centro = Centros.get(x);
                  ArrayList<Plaza> plazas = centro.getPlazas(Docentes.get(i).getArea());
                  
                  if(plazas.size()>0)
                  {
                    int ale1= (int) (Math.random()*plazas.size()) ;

                    plazas.get(ale1).addInteresado(Docentes.get(i));
                  }
              }
              
              for (int x=0;x<inicio;x++)
              {
                  //verifica si hay un plaza adecuada para el docente (area y futuramente la cantidad de periodos)
                  CentroEducativo centro = Centros.get(x);
                  ArrayList<Plaza> plazas = centro.getPlazas(Docentes.get(i).getArea());
                  
                  if(plazas.size()>0)
                  {
                    int ale1= (int) (Math.random()*plazas.size()) ;

                    plazas.get(ale1).addInteresado(Docentes.get(i));
                  }
              }
        }
    }
}
