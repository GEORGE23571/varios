
package modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Plaza 
{
    private String area;// nombre del area, MAT, CS,CYL,CNN
    private String tipo;//si es nombramiento de 30 periodo o menos:
    private int nombramiento;//cantidad de plazas del area y del tipo de nombramiento
    private boolean ocupada;//indica si la plaza esta libre o aun en busca de docente 
    private ArrayList<Docente> interesados = new ArrayList<Docente>  ();
    private ArrayList<Docente> convocados =  new ArrayList<Docente>  ();//lista de docnetes a entrevistar

    public void sortInteresados()//ordena a los interesados por un puesto de acuerdo a la calificacion obtenida
    {
       Collections.sort(interesados,new Comparator() {
           @Override
           public int compare(Object o1, Object o2) 
           {
           return new Integer(((Docente)o2).getCalificacion()).compareTo(new Integer(((Docente)o1).getCalificacion())); 
           }
       });
    }
    
   
    public ArrayList<Docente> getInteresados()
    {
        return this.interesados;
    }
    public void addInteresado(Docente doc)
    {
        interesados.add(doc);
    }
    
    public String getArea() {
        return area;
    }

    public String getTipo() {
        return tipo;
    }

    public int getNombramiento()
    {
        return nombramiento;
    }

    public void setArea(String area)
    {
        this.area = area;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setNombramiento(int nombramiento) {
        this.nombramiento = nombramiento;
    }

    public boolean isOcupada() {
        return ocupada;
    }

    public void setOcupada(boolean ocupada) {
        this.ocupada = ocupada;
    }

    public void setInteresados(ArrayList<Docente> interesados) {
        this.interesados = interesados;
    }

    public ArrayList<Docente> getConvocados() {
        return convocados;
    }

    public void setConvocados(ArrayList<Docente> convocados) {
        this.convocados = convocados;
    }
    public void addConvocado(Docente convocado) //agrega docente convocado
    {
        this.convocados.add(convocado);
    }
    public void removeAllConvocados()
    {
        for (int i = 0;i<this.convocados.size();i++)
        {
            this.convocados.get(i).setEstado(0);//liberar para nueva entrevista
        }
        this.convocados.clear();
    }
    public Docente getConvocado(int i )
    {
        return this.convocados.get(i);
    }
    
    public int getNoInteresados()
    {//retorna numeros  de interesados validos
    int n=0;
    int k = this.interesados.size();
    for (int i = 0; i<k;i++)
    {
       if(this.interesados.get(i).getEstado()==0)
       {
           n++;
       }
    }
    return n;
    }
    
}
