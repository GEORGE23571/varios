
package modelo;

import java.util.ArrayList;


public class Municipio 
{
    private final int id_municipio ;
    private final int id_departamento;
    
     private ArrayList<CentroEducativo> centros = new ArrayList <CentroEducativo>();
                                                                          //LISTA DE DOCNETES POR ESPECIALIDADES
     public ArrayList<Docente>   docentesMAT = new ArrayList <Docente>();
     public ArrayList<Docente>   docentesCYL = new ArrayList <Docente>();
     public ArrayList<Docente>   docentesCNN = new ArrayList <Docente>();
     public ArrayList<Docente>   docentesCS = new ArrayList <Docente>();
     
     
    public Municipio(int id_municipio, int id_departamento)
    {
        this.id_departamento= id_departamento;
        this.id_municipio= id_municipio;
    }
    
    public void addDocente(Docente doc)
    {
        switch (doc.getArea()) {
            case "MAT":
                this.docentesMAT.add(doc);
                break;
            case "CYL":
                this.docentesCYL.add(doc);
                break;
            case "CNN":
                this.docentesCNN.add(doc);
                break;
            case "CS":
                this.docentesCS.add(doc);
                break;
            default:
                break;
        }
       
    }
    
    public void addCentro(CentroEducativo ce)
    {
        centros.add(ce);
    }

    public int getId_municipio() {
        return id_municipio;
    }

    public int getId_departamento() {
        return id_departamento;
    }

    public ArrayList<CentroEducativo> getCentros() {
        return centros;
    }

    public void setCentros(ArrayList<CentroEducativo> centros) {
        this.centros = centros;
    }
    
    
    
}
