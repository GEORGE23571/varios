
package modelo;

import java.util.logging.Logger;


public class Iteracion 
{
private int cantidad_entrevistas;
private int plazas;
private int iteracion ;
    public int getCantidad_entrevistas() {
        return cantidad_entrevistas;
    }

    public int getPlazas() {
        return plazas;
    }

    public void setCantidad_entrevistas(int cantidad_entrevistas) {
        this.cantidad_entrevistas = cantidad_entrevistas;
    }

    public void setPlazas(int plazas) {
        this.plazas = plazas;
    }

    public Iteracion(int cantidad_entrevistas, int plazas, int iter) {
        this.cantidad_entrevistas = cantidad_entrevistas;
        this.plazas = plazas;
        this.iteracion=iter;
    }

    public int getIteracion() {
        return iteracion;
    }

    public void setIteracion(int iteracion) {
        this.iteracion = iteracion;
    }


}
