
package modelo;

import java.util.ArrayList;

public class Departamento 
{
    
    private int id_departamento;
    public  ArrayList<Municipio> municipios = new ArrayList<Municipio>();
   
    
    
    
    public Departamento (int id)
    {
    this.id_departamento= id;
    }
    
    public void add_municipio(Municipio muni)
    {
        this.municipios.add(muni);//agrega al municipio
    }

    public int getId_departamento() {
        return id_departamento;
    }

    public Municipio getMunicipio(int id_muni) 
    {
        int k =municipios.size();
        for(int i = 0;i < k ; i++)
        {
           if(municipios.get(i).getId_municipio()==id_muni)
           {
               return municipios.get(i);
           }
        }
        
        Municipio muni= new Municipio(id_muni,id_departamento);
        this.municipios.add(muni);
        
        return muni;
    }

    public void setId_departamento(int id_departamento) {
        this.id_departamento = id_departamento;
    }

    public void setMunicipio(ArrayList<Municipio> municipio) {
        this.municipios = municipio;
    }
    
    
    
}
