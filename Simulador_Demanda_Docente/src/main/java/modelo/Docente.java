
package modelo;

import com.mycompany.simulador_demanda_docente.Simulador;

public class Docente implements Comparable<Docente>
{

private int id_docente;//identificador de docente
private int calificacion;//metrica de evaluacion
private String area;// nombre del area: MAT, CS,CYL,CNN
private int periodos;//periodos asignados

private int municipio;
private int departamento;

private int estado=0;//0 libre,1 ocupado para entrevista, 2 contratado

    public int getId_docente() {
        return id_docente;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public int getPeriodos() {
        return periodos;
    }

    public int getMunicipio() {
        return municipio;
    }

    public int getDepartamento() {
        return departamento;
    }

    public String getArea() {
        return area;
    }

    public void setId_docente(int id_docente) {
        this.id_docente = id_docente;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setPeriodos(int periodos) {
        this.periodos = periodos;
    }

    public void setMunicipio(int municipio) {
        this.municipio = municipio;
    }

    public void setDepartamento(int departamento) {
        this.departamento = departamento;
    }

    
    
    
    
    
    @Override
    public int compareTo(Docente o) 
    {
        if(this.calificacion<o.getCalificacion())
        {
        return -1;
        }
        else if(this.calificacion>o.getCalificacion())
        {
        return 1;
        }
       return 0;
    }

    public int getEstado() {
        return this.estado;
    }

    public void setEstado(int estado) 
    {
        this.estado = estado;
        if(this.estado==2)
        {
           Simulador.suma_puestos();
        }
    }




    
}