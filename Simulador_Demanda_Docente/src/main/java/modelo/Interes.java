
package modelo;

public class Interes
{
    private Docente docente;
    private int prioridad;//indica la prioridad que tiene 

    public Docente getDocente() {
        return docente;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public Interes(Docente docente, int prioridad)
    {
        this.docente = docente;
        this.prioridad = prioridad;
    }
    
 
}
