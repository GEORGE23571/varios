
package modelo;

import java.util.ArrayList;

public class CentroEducativo 
{
    private String  codigo;//codigo del centro educativo
    
    private double  measure;//calificacion obtenida de DIGEDUCA
    private int     ratio;//cantidad de alumnos por cada maestro en el centro
    
    private ArrayList<Plaza> plazas = new ArrayList<Plaza>();
    
    private int departamento;
    private int municipio;
    
    private ArrayList<Integer> entrevistas = new ArrayList<Integer>();//contiene el numero de entrevistas en cada iteracion
    
    
    public ArrayList<Plaza> getPlazas(String area)
    {
        ArrayList<Plaza> result = new ArrayList<Plaza>();
        int k = plazas.size();
        
        for(int i=0;i<k;i++)
        {
            if(plazas.get(i).getArea().equals(area))
            {
               result.add(plazas.get(i));
            }
        }
    
        return result;
    }
    
    public Plaza getPlaza(int i )
    {
        return plazas.get(i);
    }
    
    public String getCodigo() 
    {
        return codigo;
    }

    public double getMeasure() {
        return measure;
    }

    public int getRatio()
    {
        return ratio;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
        //asignar departamento y municipio 
        this.setDepartamento(Integer.parseInt(codigo.substring(0, 2)));
        this.setMunicipio(Integer.parseInt(codigo.substring(3, 5)));
    }

    public void setMeasure(double measure)
    {
        this.measure = measure;
    }

    public void setRatio(int ratio) 
    {
        this.ratio = ratio;
    }

    public ArrayList<Plaza> getPlazas()
    {
        return plazas;
    }

    public void setPlazas( Plaza plaza)
    {//insercion de elemento plaza.
        this.plazas.add(plaza);
    }

    public int getDepartamento() {
        return departamento;
    }

    public int getMunicipio() {
        return municipio;
    }

    public void setPlazas(ArrayList<Plaza> plazas)
    {
        this.plazas = plazas;
    }

    public void setDepartamento(int departamento) {
        this.departamento = departamento;
    }

    public void setMunicipio(int municipio) {
        this.municipio = municipio;
    }

    public ArrayList<Integer> getEntrevistas() {
        return entrevistas;
    }

    public void setEntrevistas(ArrayList<Integer> entrevistas) {
        this.entrevistas = entrevistas;
    }
   
    public void setEntrevistas(int n)
    {
        this.entrevistas.add(n);//inserta el valor de n al final de la lista
    }
    public int getPlazasLibres()
    {
        int m = 0;
        for (int i = 0;i<this.plazas.size();i++)
        {
            if(!this.plazas.get(i).isOcupada())
            {
                for(int j = 0;j<this.plazas.get(i).getInteresados().size();j++)
                {
                    if(this.plazas.get(i).getInteresados().get(j).getEstado()==0)//hay interesados libres
                    {
                     m++;
                     break;
                    }
                   
                }
            
            }
        }
        return m;
    }


}
