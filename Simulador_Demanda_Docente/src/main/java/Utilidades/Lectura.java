
package Utilidades;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import modelo.CentroEducativo;
import modelo.Departamento;
import modelo.Docente;
import modelo.Municipio;
import modelo.Plaza;
 
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
 

public class Lectura 
{
    public static ArrayList<Docente> Docentes; 
    public static ArrayList<CentroEducativo> Centros;
    
    public static ArrayList<Departamento> Departamentos;
    
    
    
    
    public static ArrayList<CentroEducativo> LeerExcel()
    {
        ArrayList<CentroEducativo> centros = new ArrayList<>();
        
	String rutaArchivo =  "Datos.xlsx";
	
        try 
        {
            Departamentos= new ArrayList<Departamento>();
            
            FileInputStream file = new FileInputStream(new File(rutaArchivo));

            XSSFWorkbook worbook = new XSSFWorkbook(file);
            //                                               CARGA DE CENTROS EDUCATIVOS
            XSSFSheet sheet = worbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
 
            Row row;// se recorre cada fila hasta el final
            row = rowIterator.next();//ignorar encabezado
            while (rowIterator.hasNext()) 
            {
                row = rowIterator.next();//se obtiene las celdas por fila
		Iterator<Cell> cellIterator = row.cellIterator();
		Cell cell;
                
                CentroEducativo ce = new CentroEducativo();
                
                cell = cellIterator.next();
                ce.setCodigo(cell.getStringCellValue());
                
                cell= cellIterator.next();
                ce.setMeasure(cell.getNumericCellValue());
                
                cell= cellIterator.next();
                ce.setRatio((int)cell.getNumericCellValue());
                addCentro(ce);//desagregar en departamento y municipio
                centros.add(ce);
            }
            
            //                                                      CARGA DE PUESTOS EN EL CENTRO
            sheet = worbook.getSheetAt(1);
            rowIterator = sheet.iterator();
            row = rowIterator.next();//ignorar encabezado
            int i=0;//los codigo de los centro viene ordenados en el archivo excel para facilidad de relacion
            
            while (rowIterator.hasNext()) 
            {
                row = rowIterator.next();//se obtiene las celdas por fila
                Iterator<Cell> cellIterator = row.cellIterator();
		Cell cell;
                cell = cellIterator.next();
                String Codigo = cell.getStringCellValue();
                    //crea la plaza temporal 
                    Plaza plaza = new Plaza();
                    
                    cell = cellIterator.next();
                    plaza.setArea(cell.getStringCellValue().trim());
                    
                    cell = cellIterator.next();
                    plaza.setTipo(cell.getStringCellValue().trim());
                    
                    cell = cellIterator.next();
                    plaza.setNombramiento((int)cell.getNumericCellValue());
                
                if (!Codigo.equals(centros.get(i).getCodigo()))
                {  //asociar a lista correpondiente
                    i++;
                }
                centros.get(i).setPlazas(plaza);
            }
            
            //                                                            CARGA DE DOCENTES SOLICITANTES
            Docentes = new ArrayList<Docente>();
            
            sheet = worbook.getSheetAt(2);
            rowIterator = sheet.iterator();
            row = rowIterator.next();//ignorar encabezado
            while (rowIterator.hasNext()) 
            {
                row = rowIterator.next();//se obtiene las celdas por fila
                Iterator<Cell> cellIterator = row.cellIterator();
		Cell cell;
                Docente doc = new Docente();
                
                cell = cellIterator.next();
                doc.setDepartamento((int)cell.getNumericCellValue());
                
                cell = cellIterator.next();
                doc.setMunicipio((int)cell.getNumericCellValue());
                
                cell = cellIterator.next();
                doc.setId_docente((int)cell.getNumericCellValue());
                
                cell = cellIterator.next();
                doc.setArea(cell.getStringCellValue());
                
                cell = cellIterator.next();
                doc.setPeriodos((int)cell.getNumericCellValue());
                
                cell = cellIterator.next();
                doc.setCalificacion((int)cell.getNumericCellValue());
                
                addDocente(doc);//agregarlo al municipio correpondiente
                
                Docentes.add(doc);//agregarlo a la lista de docentes
            }
        }
        catch (Exception e) 
        {
            System.out.println("._. "+e.getMessage());
	}
       Lectura.Centros= centros;
      
       return centros;
    }
    
    
    private static Departamento getDepartamento(int id)
    {
        
        int k = Departamentos.size();
        for(int i =0; i<k;i++)
        {
            if (Departamentos.get(i).getId_departamento()==id)
            {
                return Departamentos.get(i);
            }
        }
        //no se encuentra en la lista; se agrega
        Departamento depto = new Departamento(id);
        Departamentos.add(depto);
        
        return depto;
    }
    
    private static void addCentro(CentroEducativo ce)
    {
        int id_departamento= Integer.parseInt(ce.getCodigo().substring(0, 2));
        int id_municipio = Integer.parseInt(ce.getCodigo().substring(3, 5));
        
        //Obtener Departamento 
        Departamento depto = getDepartamento(id_departamento);
        
        Municipio muni =depto.getMunicipio(id_municipio);
        
        muni.addCentro(ce);//agrega al municipio el centro educativo
    }
    
    
    private static void addDocente(Docente doc)
    {
       int id_departamento = doc.getDepartamento();
       int id_municipio= doc.getMunicipio();
       
       Departamento depto = getDepartamento(id_departamento);
       Municipio muni =depto.getMunicipio(id_municipio);
       
       muni.addDocente(doc);
       
    }
    
}
