/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2ia;

import java.util.Arrays;

 public class li
{
 public int [] lista=new int[12];
 public li(int [] l)
 {
     System.arraycopy(l, 0, lista, 0, 12);
 }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Arrays.hashCode(this.lista);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final li other = (li) obj;
        if (!Arrays.equals(this.lista, other.lista)) {
            return false;
        }
        return true;
    }
 
 
} 
   