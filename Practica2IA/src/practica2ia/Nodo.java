
package practica2ia;

import java.util.ArrayList;
import java.util.Arrays;

public class Nodo implements Comparable <Nodo>
{
   int [] lista =new int[12];
    private  ArrayList<Nodo> hijos= new ArrayList<>();
    int f=0;
    int g=0;
    private int h=0;
    String recorrido;
    
    public Nodo(int [] l, int altura, String cadena )
    {
        System.arraycopy(l, 0, lista, 0, 12); 
        g= altura;
        h();//calcula heuristica h
        f=h+g;
        recorrido = cadena +"  " +Arrays.toString(lista)+"  /G(n) "+g+"  /F(n)  "+f+"\n";
    }
    public boolean IsNodoFinal()
    {
      for (int i =0;i<11;i++)
      {
         if(lista[i]!=(i+1))
         {
           return false;
         }
      }
    return true;
    }
    
   public ArrayList<Nodo> hijos()
   {
        if(g<Interfaz.G)
        {
          sucesores();
        }
   return hijos;
   
   }
    
    private void h()
    {
        int saltos=0;
    //Manhattan: sumatoria de los saltos que le faltan a todas las piezas para llegar a su lugar
     switch (lista[0])
     {
        case 1:
             saltos+=0;break;
        case 2:
             saltos+=1;break;
        case 3:
             saltos+=2;break;
        case 4:
             saltos+=3;break;
        case 5:
             saltos+=4;break;
        case 6:
             saltos+=3;break;
        case 7:
             saltos+=2;break;
        case 8:
             saltos+=1;break;
             
        case 9:
             saltos+=1;break;
        case 10:
             saltos+=2;break;
        case 11:
             saltos+=3;break;
        case -1:
             saltos+=2;break;
     }
         switch (lista[1])
     {
        case 1:
             saltos+=1;break;
        case 2:
             saltos+=0;break;
        case 3:
             saltos+=1;break;
        case 4:
             saltos+=2;break;
        case 5:
             saltos+=3;break;
        case 6:
             saltos+=4;break;
        case 7:
             saltos+=3;break;
        case 8:
             saltos+=2;break;
             
        case 9:
             saltos+=1;break;
        case 10:
             saltos+=2;break;
        case 11:
             saltos+=3;break;
        case -1:
             saltos+=2;break;
     }
 switch (lista[2])
     {
        
        case 1:
             saltos+=2;break;
        case 2:
             saltos+=1;break;
        case 3:
             saltos+=0;break;
        case 4:
             saltos+=1;break;
        case 5:
             saltos+=2;break;
        case 6:
             saltos+=3;break;
        case 7:
             saltos+=4;break;
        case 8:
             saltos+=3;break;
             
        case 9:
             saltos+=2;break;
        case 10:
             saltos+=1;break;
        case 11:
             saltos+=2;break;
        case -1:
             saltos+=3;break;
     }
 switch (lista[3])
     {
        
        case 1:
             saltos+=3;break;
        case 2:
             saltos+=2;break;
        case 3:
             saltos+=1;break;
        case 4:
             saltos+=0;break;
        case 5:
             saltos+=1;break;
        case 6:
             saltos+=2;break;
        case 7:
             saltos+=3;break;
        case 8:
             saltos+=4;break;
             
        case 9:
             saltos+=2;break;
        case 10:
             saltos+=1;break;
        case 11:
             saltos+=2;break;
        case -1:
             saltos+=3;break;
     }
  switch (lista[4])
     {
        
        case 1:
             saltos+=4;break;
        case 2:
             saltos+=3;break;
        case 3:
             saltos+=2;break;
        case 4:
             saltos+=1;break;
        case 5:
             saltos+=0;break;
        case 6:
             saltos+=1;break;
        case 7:
             saltos+=2;break;
        case 8:
             saltos+=3;break;
             
        case 9:
             saltos+=3;break;
        case 10:
             saltos+=2;break;
        case 11:
             saltos+=1;break;
        case -1:
             saltos+=2;break;
     }
   switch (lista[5])
     {
        
        case 1:
             saltos+=3;break;
        case 2:
             saltos+=4;break;
        case 3:
             saltos+=3;break;
        case 4:
             saltos+=2;break;
        case 5:
             saltos+=1;break;
        case 6:
             saltos+=0;break;
        case 7:
             saltos+=1;break;
        case 8:
             saltos+=2;break;
             
        case 9:
             saltos+=3;break;
        case 10:
             saltos+=2;break;
        case 11:
             saltos+=1;break;
        case -1:
             saltos+=2;break;
     }
switch (lista[6])
     {
        
        case 1:
             saltos+=2;break;
        case 2:
             saltos+=3;break;
        case 3:
             saltos+=4;break;
        case 4:
             saltos+=3;break;
        case 5:
             saltos+=2;break;
        case 6:
             saltos+=1;break;
        case 7:
             saltos+=0;break;
        case 8:
             saltos+=1;break;
             
        case 9:
             saltos+=2;break;
        case 10:
             saltos+=3;break;
        case 11:
             saltos+=2;break;
        case -1:
             saltos+=1;break;
     }
switch (lista[7])
     {
        
        case 1:
             saltos+=1;break;
        case 2:
             saltos+=2;break;
        case 3:
             saltos+=3;break;
        case 4:
             saltos+=4;break;
        case 5:
             saltos+=3;break;
        case 6:
             saltos+=2;break;
        case 7:
             saltos+=1;break;
        case 8:
             saltos+=0;break;
             
        case 9:
             saltos+=2;break;
        case 10:
             saltos+=3;break;
        case 11:
             saltos+=2;break;
        case -1:
             saltos+=1;break;
     }
switch (lista[8])
     {
        
        case 1:
             saltos+=1;break;
        case 2:
             saltos+=1;break;
        case 3:
             saltos+=2;break;
        case 4:
             saltos+=2;break;
        case 5:
             saltos+=3;break;
        case 6:
             saltos+=3;break;
        case 7:
             saltos+=2;break;
        case 8:
             saltos+=2;break;
             
        case 9:
             saltos+=0;break;
        case 10:
             saltos+=1;break;
        case 11:
             saltos+=2;break;
        case -1:
             saltos+=1;break;
     }
switch (lista[9])
     {
        
        case 1:
             saltos+=2;break;
        case 2:
             saltos+=2;break;
        case 3:
             saltos+=1;break;
        case 4:
             saltos+=1;break;
        case 5:
             saltos+=2;break;
        case 6:
             saltos+=2;break;
        case 7:
             saltos+=3;break;
        case 8:
             saltos+=3;break;
             
        case 9:
             saltos+=1;break;
        case 10:
             saltos+=0;break;
        case 11:
             saltos+=1;break;
        case -1:
             saltos+=2;break;
     }
switch (lista[10])
     {
        
        case 1:
             saltos+=3;break;
        case 2:
             saltos+=3;break;
        case 3:
             saltos+=2;break;
        case 4:
             saltos+=2;break;
        case 5:
             saltos+=1;break;
        case 6:
             saltos+=1;break;
        case 7:
             saltos+=2;break;
        case 8:
             saltos+=2;break;
             
        case 9:
             saltos+=2;break;
        case 10:
             saltos+=1;break;
        case 11:
             saltos+=0;break;
        case -1:
             saltos+=1;break;
     }
switch (lista[11])
     {
        
        case 1:
             saltos+=2;break;
        case 2:
             saltos+=2;break;
        case 3:
             saltos+=3;break;
        case 4:
             saltos+=3;break;
        case 5:
             saltos+=2;break;
        case 6:
             saltos+=2;break;
        case 7:
             saltos+=1;break;
        case 8:
             saltos+=1;break;
             
        case 9:
             saltos+=1;break;
        case 10:
             saltos+=2;break;
        case 11:
             saltos+=1;break;
        case -1:
             saltos+=0;break;
     }
   this.h=saltos;
   
    //casillas fuera de lugar 
       for(int i =0;i<11;i++)
       {
         if(this.lista[i]!=(i+1))
         {
          this.h++;
         }
       }
       if (this.lista[11]!=-1)
       {
         this.h++;
       }
    
    
    }
    
    private void sucesores ()
    {
        int [] l=new int[12];
    //encontrar el lugar donde esta el espacio vacio
        int i=0;
        for(;i<11;i++)
        {
           if(lista[i]==-1)
           {
             break;
           }
        }
    // generar hijos
       System.arraycopy(lista, 0, l, 0, 12);
    
        switch(i)
        {
            case 0:
                l[0]=lista[1]; l[1]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[0]=lista[7]; l[7]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[0]=lista[8]; l[8]=-1;
                break;
            case 1:
                l[1]=lista[2]; l[2]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[1]=lista[0]; l[0]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[1]=lista[8]; l[8]=-1;
                break;
            case 2:
                l[2]=lista[3]; l[3]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[2]=lista[1]; l[1]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[2]=lista[9]; l[9]=-1;
                break;
            case 3:
                l[3]=lista[4]; l[4]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[3]=lista[2]; l[2]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[3]=lista[9]; l[9]=-1;
                break;
            case 4:
                l[4]=lista[5]; l[5]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[4]=lista[3]; l[3]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[4]=lista[10]; l[10]=-1;
                break;
            case 5:
                l[5]=lista[6]; l[6]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[5]=lista[4]; l[4]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[5]=lista[10]; l[10]=-1;
                break;
            case 6:
                l[6]=lista[7]; l[7]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[6]=lista[5]; l[5]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[6]=lista[11]; l[11]=-1;
                break;
            case 7:
                l[7]=lista[0]; l[0]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[7]=lista[6]; l[6]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[7]=lista[11]; l[11]=-1;
                break;
            case 8:
                l[8]=lista[0]; l[0]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[8]=lista[1]; l[1]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[8]=lista[11]; l[11]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[8]=lista[9]; l[9]=-1;
                break;
            case 9:
                l[9]=lista[2]; l[2]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[9]=lista[3]; l[3]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[9]=lista[8]; l[8]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[9]=lista[10]; l[10]=-1;
                break;
            case 10:
                l[10]=lista[4]; l[4]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[10]=lista[5]; l[5]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[10]=lista[9]; l[9]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[10]=lista[11]; l[11]=-1;
                break;
            case 11:
                l[11]=lista[6]; l[6]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[11]=lista[7]; l[7]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[11]=lista[10]; l[10]=-1;
                this.hijos.add(new Nodo(l,this.g+1,recorrido));
                System.arraycopy(lista, 0, l, 0, 12);
                l[11]=lista[8]; l[8]=-1;
                break;
        
        }
    this.hijos.add(new Nodo(l,this.g+1,recorrido));
    
    int j=0;
    for( i=0;i<hijos.size();i++)
    {
       j=Interfaz.visitados.size();
       Interfaz.visitados.add(new li(hijos.get(i).lista));
       
       if(j==Interfaz.visitados.size())
       {
           hijos.remove(i);
           i--;
       }
    }
     
    }

   


  

    @Override
    public int compareTo(Nodo o) 
    
    {
         if (f < o.f) {
                return -1;
            }
            if (f > o.f) {
                return 1;
            }
            return 0;
  }
    
    
   
   
   
   
   
   
}

